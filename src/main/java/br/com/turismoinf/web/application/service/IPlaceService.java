package br.com.turismoinf.web.application.service;

import java.util.List;

import br.com.turismoinf.web.domain.models.place.Place;

public interface IPlaceService {

	public Place createPlace(String name, String descripton, int cep, String city, String street, int number, List<Long> categories, List<String> images,
			String email, Long phone);
	
	public void updatePlace(long id, String name, String descripton, int cep, String city, String street, int number, List<Long> categories, List<String> images,
			String email, Long phone);
	
	public void deletePlace(long id);
	
	List<String> canCreate(String name, String descripton, int cep, String city, String street, int number, List<Long> categories,
			String email, Long phone);
	
	List<String> canUpdate(long id, String name, String descripton, int cep, String city, String street, int number, List<Long> categories,
			String email, Long phone);
	
	List<String> canDelete(long id);
}
