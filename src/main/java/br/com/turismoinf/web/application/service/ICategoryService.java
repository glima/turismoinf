package br.com.turismoinf.web.application.service;

import java.util.List;

import br.com.turismoinf.web.domain.models.category.Category;

public interface ICategoryService {

	public Category createCategory(String descripton, String icon);
	
	public void updateCategory(long id, String descripton, String icon);
	
	public void deleteCategory(long id);
	
	List<String> canCreate(String descripton);
	
	List<String> canUpdate(long id, String descripton);
	
	List<String> canDelete(long id);
}
