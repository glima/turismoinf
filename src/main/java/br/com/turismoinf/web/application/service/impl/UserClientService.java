package br.com.turismoinf.web.application.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.turismoinf.web.application.service.IUserClientService;
import br.com.turismoinf.web.domain.models.user.IUserClientRepository;
import br.com.turismoinf.web.domain.models.user.UserClient;

@Service
public class UserClientService implements IUserClientService {

	@Inject IUserClientRepository userRepository;
	
	@Transactional(propagation=Propagation.REQUIRED)
	public UserClient createUser(String name, Long phone, String password, String email) {
		
		if(!this.canCreate(name, phone, password, email).isEmpty()) {
			throw new IllegalStateException("Dados inválidos para a operação.");
		}
		
		Md5PasswordEncoder encoder = new Md5PasswordEncoder();
		
		UserClient user = new UserClient(name, email, encoder.encodePassword(password, null));
		user.setPhone(phone);
		
		user = this.userRepository.save(user);
		
		return user;
	}
	
	public List<String> canCreate(String name, Long phone, String password, String email) {
		List<String> errors = new ArrayList<String>();
		
		errors.addAll(this.verifyFields(name, phone, password, email));
		
		UserClient userByEmail = this.userRepository.findByEmail(email);
		
		if(StringUtils.isBlank(password)) {
			errors.add("Senha de usuário obrigatório.");
		}
		
		if(userByEmail != null) {
			errors.add("Email já existente.");
		}
		
		return errors;
	}
	
	private List<String> verifyFields(String name, Long phone, String password, String email) {
		List<String> errors = new ArrayList<String>();
		
		if(StringUtils.isBlank(name)) {
			errors.add("Nome de usuário obrigatório.");
		}
		
		if(phone != null && phone.toString().length() != 11) {
			errors.add("Telefone inválido. Informe um telefone junto ao DDD. Ex: 3199887766.");
		}
		
		if(StringUtils.isBlank(email) || !EmailValidator.getInstance().isValid(email)) {
			errors.add("Email de usuário inválido.");
		}
		
		return errors;
	}

	public List<String> canDelete(long id) {
		List<String> errors = new ArrayList<String>();
		UserClient userObj = this.userRepository.findById(id);
		
		if(userObj == null) {
			errors.add("Usuário não encontrado.");
		}
		
		return errors;
	}

	public List<String> canResetPassword(long id, String password, String newPassword) {
		List<String> errors = new ArrayList<String>();

		UserClient user = this.userRepository.findById(id);
		if(user == null) {
			errors.add("Usuário não encontrado.");
		} else {
			Md5PasswordEncoder encoder = new Md5PasswordEncoder();
			
			if(password == null) {
				errors.add("Senha atual obrigatória.");
			} else if(!encoder.encodePassword(password, null).equals(user.getPassword())) {
				errors.add("Senha incorreta.");
			}
			
			if(newPassword == null) {
				errors.add("Nova senha obrigatória.");
			}
		}
		
		return errors;
	}

	public void resetPassword(long id, String password, String newPassword) {
		if(!this.canResetPassword(id, password, newPassword).isEmpty()) {
			throw new IllegalStateException("Dados inválidos para a operação.");
		}
		
		UserClient user = this.userRepository.findById(id);
		Md5PasswordEncoder encoder = new Md5PasswordEncoder();
		user.setPassword(encoder.encodePassword(newPassword, null));
		
		this.userRepository.saveAndFlush(user);
	}
	
}
