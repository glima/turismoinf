package br.com.turismoinf.web.application.service;

import java.util.List;

import br.com.turismoinf.web.domain.models.conf.ContactConf;
import br.com.turismoinf.web.domain.models.conf.EmailConf;
import br.com.turismoinf.web.domain.models.conf.InitialConf;

public interface IConfigurationService {

	public void updateConfiguration(EmailConf emailConf, ContactConf contactConf, InitialConf initialConf);
	
	public List<String> canUpdateConfiguration(EmailConf emailConf, ContactConf contactConf, InitialConf initialConf);
}
