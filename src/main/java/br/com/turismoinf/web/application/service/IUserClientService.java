package br.com.turismoinf.web.application.service;

import java.util.List;

import br.com.turismoinf.web.domain.models.user.UserClient;

public interface IUserClientService {

	public UserClient createUser(String name, Long phone, String password, String email);
	
	List<String> canCreate(String name, Long phone, String password, String email);
	
	public List<String> canResetPassword(long id, String password, String newPassword);

	public void resetPassword(long id, String password, String newPassword);
	
}
