package br.com.turismoinf.web.application.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.inject.Inject;

import org.springframework.stereotype.Service;

import br.com.turismoinf.web.application.service.IConfigurationService;
import br.com.turismoinf.web.domain.models.conf.ContactConf;
import br.com.turismoinf.web.domain.models.conf.EmailConf;
import br.com.turismoinf.web.domain.models.conf.InitialConf;
import br.com.turismoinf.web.infra.dao.impl.ConfigurationDao;

@Service
public class ConfigurationService implements IConfigurationService {

	@Inject @Resource(name="confDao") private ConfigurationDao confDao;
	
	@Override
	public void updateConfiguration(EmailConf emailConf, ContactConf contactConf, InitialConf initialConf) {
		if(!this.canUpdateConfiguration(emailConf, contactConf, initialConf).isEmpty()) {
			throw new IllegalStateException("Dados inválidos para a operação.");
		}
		
		Map<String, String> conf = new HashMap<>();
		if(emailConf != null) {
			conf.put("email_servidor", emailConf.getServer());
			conf.put("email_porta", String.valueOf(emailConf.getPort()));
			conf.put("email_conta", emailConf.getEmail());
			conf.put("email_senha", emailConf.getPassword());			
		} else {
			conf.put("email_servidor", null);
			conf.put("email_porta", null);
			conf.put("email_conta", null);
			conf.put("email_senha", null);
		}
		
		if(initialConf != null) {
			conf.put("imagem_inicial", initialConf.getPhoto());
			conf.put("descricao_inicial", initialConf.getDescription());			
		} else {
			conf.put("imagem_inicial", null);
			conf.put("descricao_inicial", null);	
		}
		
		if(contactConf != null) {
			conf.put("email_contato", contactConf.getEmail());
			conf.put("telefone_contato", contactConf.getPhone() == null ? null : contactConf.getPhone().toString());			
		} else {
			conf.put("email_contato", null);
			conf.put("telefone_contato", null);	
		}
		
		this.confDao.updateEmailConfiguration(conf);
	}

	@Override
	public List<String> canUpdateConfiguration(EmailConf emailConf, ContactConf contactConf, InitialConf initialConf) {
		List<String> errors = new ArrayList<>();
		
		if(emailConf == null && contactConf == null && initialConf == null) {
			errors.add("A configuração não pode ser vazia.");
		} else {
			if(emailConf != null && !emailConf.isValid()) {
				errors.add("Configuração de email inválida.");
			}
			
			if(contactConf != null && !contactConf.isValid()) {
				errors.add("Configuração de contato inválida.");
			}
			
			if(initialConf != null && !initialConf.isValid()) {
				errors.add("Descrição de configuração inicial deve ser informada.");
			}
		}
		
		return errors;
	}

}
