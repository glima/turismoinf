package br.com.turismoinf.web.application.service;

import java.util.List;

import br.com.turismoinf.web.domain.models.user.User;

public interface IUserService {

	public User createUser(String name, String login, String password, String email);
	
	public void updateUser(long id, String name, String login, String password, String email);
	
	public void deleteUser(long id);
	
	public List<String> canUpdateStatus(long id);
	
	List<String> canCreate(String name, String login, String password, String email);
	
	List<String> canUpdate(long id, String name, String login, String password, String email);
	
	List<String> canDelete(long id);

	public List<String> canResetPassword(long id, String password, String newPassword);

	public void resetPassword(long id, String password, String newPassword);
	
	public void enableUser(long id);
	
	public void disableUser(long id);

}
