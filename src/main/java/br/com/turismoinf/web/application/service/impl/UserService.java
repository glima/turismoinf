package br.com.turismoinf.web.application.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.turismoinf.web.application.service.IUserService;
import br.com.turismoinf.web.domain.models.user.IUserRepository;
import br.com.turismoinf.web.domain.models.user.User;

@Service
public class UserService implements IUserService {

	@Inject IUserRepository userRepository;
	
	@Transactional(propagation=Propagation.REQUIRED)
	public User createUser(String name, String login, String password, String email) {
		
		if(!this.canCreate(name, login, password, email).isEmpty()) {
			throw new IllegalStateException("Dados inválidos para a operação.");
		}
		
		Md5PasswordEncoder encoder = new Md5PasswordEncoder();
		
		User user = new User(login, name, email, encoder.encodePassword(password, null));
		
		user = this.userRepository.save(user);
		
		return user;
	}

	@Transactional(propagation=Propagation.REQUIRED)
	public void updateUser(long id, String name, String login, String password, String email) {

		if(!this.canUpdate(id, name, login, password, email).isEmpty()) {
			throw new IllegalStateException("Dados inválidos para a operação.");
		}
		
		User user = this.userRepository.findById(id);
		user.setEmail(email);
		user.setName(name);
		user.setLogin(login);
		
		this.userRepository.saveAndFlush(user);
	}
	
	@Override
	public List<String> canUpdateStatus(long id) {
		List<String> errors = new ArrayList<String>();
		User userObj = this.userRepository.findById(id);
		
		if(userObj == null) {
			errors.add("Usuário não encontrado.");
		}
		
		return errors;
	}

	@Transactional(propagation=Propagation.REQUIRED)
	public void deleteUser(long id) {
		if(!this.canDelete(id).isEmpty()) {
			throw new IllegalStateException("Dados inválidos para a operação.");
		}

		this.userRepository.delete(id);
	}

	public List<String> canCreate(String name, String login, String password, String email) {
		List<String> errors = new ArrayList<String>();
		
		errors.addAll(this.verifyFields(name, login, password, email));
		
		User userByEmail = this.userRepository.findByEmail(email);
		User userByLogin = this.userRepository.findByUsername(login);
		
		if(StringUtils.isBlank(password)) {
			errors.add("Senha de usuário obrigatório.");
		}
		
		if(userByEmail != null) {
			errors.add("Email indisponível para uso.");
		}
		
		if(userByLogin != null) {
			errors.add("Login indisponível para uso.");
		}
		
		return errors;
	}
	
	private List<String> verifyFields(String name, String login, String password, String email) {
		List<String> errors = new ArrayList<String>();
		
		if(StringUtils.isBlank(name)) {
			errors.add("Nome de usuário obrigatório.");
		}
		
		if(StringUtils.isBlank(login)) {
			errors.add("Login de usuário obrigatório.");
		}
		
		if(StringUtils.isBlank(email) || !EmailValidator.getInstance().isValid(email)) {
			errors.add("Email de usuário inválido.");
		}
		
		return errors;
	}

	public List<String> canUpdate(long id, String name, String login, String password, String email) {
		List<String> errors = new ArrayList<String>();

		User user = this.userRepository.findById(id);
		if(user == null) {
			errors.add("Usuário não encontrado.");
		} else {
			List<String> errorsFields = this.verifyFields(name, login, password, email);
			errors.addAll(errorsFields);
			
			User userByEmail = this.userRepository.findByEmail(email);
			User userByLogin = this.userRepository.findByUsername(login);
			
			if(userByEmail != null && userByEmail.getId() != user.getId()) {
				errors.add("Email indisponível para uso.");
			}
			
			if(userByLogin != null && userByLogin.getId() != user.getId()) {
				errors.add("Login indisponível para uso.");
			}
		}
		
		return errors;
	}

	public List<String> canDelete(long id) {
		List<String> errors = new ArrayList<String>();
		User userObj = this.userRepository.findById(id);
		
		if(userObj == null) {
			errors.add("Usuário não encontrado.");
		}
		
		return errors;
	}

	public List<String> canResetPassword(long id, String password, String newPassword) {
		List<String> errors = new ArrayList<String>();

		User user = this.userRepository.findById(id);
		if(user == null) {
			errors.add("Usuário não encontrado.");
		} else {
			Md5PasswordEncoder encoder = new Md5PasswordEncoder();
			
			if(password == null) {
				errors.add("Senha atual obrigatória.");
			} else if(!encoder.encodePassword(password, null).equals(user.getPassword())) {
				errors.add("Senha incorreta.");
			}
			
			if(newPassword == null) {
				errors.add("Nova senha obrigatória.");
			}
		}
		
		return errors;
	}

	public void resetPassword(long id, String password, String newPassword) {
		if(!this.canResetPassword(id, password, newPassword).isEmpty()) {
			throw new IllegalStateException("Dados inválidos para a operação.");
		}
		
		User user = this.userRepository.findById(id);
		Md5PasswordEncoder encoder = new Md5PasswordEncoder();
		user.setPassword(encoder.encodePassword(newPassword, null));
		
		this.userRepository.saveAndFlush(user);
	}
	
	@Transactional(propagation=Propagation.REQUIRED)
	@Override
	public void disableUser(long id) {
		if(!this.canUpdateStatus(id).isEmpty()) {
			throw new IllegalStateException("Dados invÃ¡lidos para a operaÃ§Ã£o.");
		}
		
		User user = this.userRepository.findById(id);
		user.disable();
		this.userRepository.save(user);
	}
	
	@Transactional(propagation=Propagation.REQUIRED)
	@Override
	public void enableUser(long id) {
		if(!this.canUpdateStatus(id).isEmpty()) {
			throw new IllegalStateException("Dados invÃ¡lidos para a operaÃ§Ã£o.");
		}
		
		User user = this.userRepository.findById(id);
		user.enable();
		this.userRepository.save(user);
	}

}
