package br.com.turismoinf.web.application.service;

import java.util.List;

import br.com.turismoinf.web.domain.models.event.Event;

public interface IEventService {

	public Event createEvent(String name, String descripton, int cep, String city, String street, int number, List<String> images,
			String email, Long phone, String dateStart, String dateEnd, String timeStart, String timeEnd, String price);
	
	public void updateEvent(long id, String name, String descripton, int cep, String city, String street, int number, List<String> images,
			String email, Long phone, String dateStart, String dateEnd, String timeStart, String timeEnd, String price);
	
	public void deleteEvent(long id);
	
	List<String> canCreate(String name, String descripton, int cep, String city, String street, int number, 
			String email, Long phone, String dateStart, String dateEnd, String timeStart, String timeEnd, String price);
	
	List<String> canUpdate(long id, String name, String descripton, int cep, String city, String street, int number, 
			String email, Long phone, String dateStart, String dateEnd, String timeStart, String timeEnd, String price);
	
	List<String> canDelete(long id);
}
