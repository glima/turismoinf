package br.com.turismoinf.web.application.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.turismoinf.web.application.service.IPlaceService;
import br.com.turismoinf.web.domain.models.category.Category;
import br.com.turismoinf.web.domain.models.category.ICategoryRepository;
import br.com.turismoinf.web.domain.models.place.IPlaceRepository;
import br.com.turismoinf.web.domain.models.place.PlacePhoto;
import br.com.turismoinf.web.domain.models.place.Place;
import br.com.turismoinf.web.utils.EmailUtil;

@Service
public class PlaceService implements IPlaceService {
	
	@Inject private IPlaceRepository placeRepository;
	@Inject private ICategoryRepository categoryRepository;
	
	private List<String> verifyFields(Long id, String name, String description, int cep, String city, String street, int number, 
			List<Long> categories,
			String email, Long phone) {
		List<String> errors = new ArrayList<>();
		
		if(categories == null || categories.isEmpty()) {
			errors.add("Categoria(s) obrigatória(s).");
		} else {
			List<Category> list = this.categoryRepository.findByIds(categories);
			
			if(list == null || list.isEmpty() || list.size() != categories.size()) {
				errors.add("Há categorias inválidas.");
			}
		}
		
		if(phone != null && phone.toString().length() != 11) {
			errors.add("Telefone inválido. Informe um telefone junto ao DDD. Ex: 3199887766.");
		}
		
		if(email != null && !EmailUtil.isValidateEmail(email)) {
			errors.add("Email de contato inválido.");
		}
		
		if(StringUtils.isBlank(name)) {
			errors.add("Nome obrigatório.");
		} else if(name.length() > 100) {
			errors.add("Nome máximo de 100 caracteres.");
		} else {
			Place one = this.placeRepository.findByName(name);
			
			if(one != null && !one.getId().equals(id)) {
				errors.add("Nome já existente.");
			}
		}
		
		if(StringUtils.isBlank(description)) {
			errors.add("Descrição obrigatório.");
		} else if(description.length() > 255) {
			errors.add("Descrição máximo de 255 caracteres.");
		}
		
		if(cep == 0) {
			errors.add("Cep obrigatório.");
		} else if(String.valueOf(cep).length() != 8) {
			errors.add("Cep inválido.");
		}
		
		if(StringUtils.isBlank(city)) {
			errors.add("Cidade obrigatório.");
		} else if(description.length() > 255) {
			errors.add("Cidade máximo de 100 caracteres.");
		}
		
		if(StringUtils.isBlank(street)) {
			errors.add("Logradouro obrigatório.");
		} else if(description.length() > 255) {
			errors.add("Logradouro máximo de 255 caracteres.");
		}
		
		if(number == 0) {
			errors.add("Número obrigatório.");
		} else if(String.valueOf(number).length() > 10) {
			errors.add("Número máximo de 10 dígitos.");
		}

		return errors;
	}

	@Transactional(propagation=Propagation.REQUIRED)
	@Override
	public Place createPlace(String name, String description, int cep, String city, String street, int number, List<Long> categories, List<String> images,
			String email, Long phone) {
		if(!this.canCreate(name, description, cep, city, street, number, categories, email, phone).isEmpty()) {
			throw new IllegalStateException("Dados inválidos para a operação.");
		}
		
		Place place = new Place();
		place.setName(name);
		place.setDescription(description);
		place.setCep(cep);
		place.setCity(city);
		place.setStreet(street);
		place.setNumber(number);
		place.setEmail(email);
		place.setPhone(phone);
		
		List<Category> list = this.categoryRepository.findByIds(categories);
		place.setCategories(new HashSet<>(list));
		
		if(images != null && !images.isEmpty()) {
			Set<PlacePhoto> photos = new HashSet<>();
			images.forEach(item -> {
				photos.add(new PlacePhoto(item, place));
			});
			
			place.setPhotos(photos);
		}
		
		Place newItem = this.placeRepository.save(place);
		
		return newItem;
	}

	@Transactional(propagation=Propagation.REQUIRED)
	@Override
	public void updatePlace(long id, String name, String description, int cep, String city, String street, int number, List<Long> categories, List<String> images,
			String email, Long phone) {
		if(!this.canUpdate(id, name, description, cep, city, street, number, categories, email, phone).isEmpty()) {
			throw new IllegalStateException("Dados inválidos para a operação.");
		}
		
		Place one = this.placeRepository.findOne(id);
		one.setName(name);
		one.setDescription(description);
		one.setCep(cep);
		one.setCity(city);
		one.setStreet(street);
		one.setNumber(number);
		one.setEmail(email);
		one.setPhone(phone);
		
		List<Category> list = this.categoryRepository.findByIds(categories);
		one.setCategories(new HashSet<>(list));
		
		this.placeRepository.deletePhotos(id);
		
		if(images != null && !images.isEmpty()) {
			Set<PlacePhoto> photos = new HashSet<>();
			images.forEach(item -> {
				photos.add(new PlacePhoto(item, one));
			});
			
			one.setPhotos(photos);
		}
		
		this.placeRepository.save(one);		
	}

	@Override
	public void deletePlace(long id) {
		if(!this.canDelete(id).isEmpty()) {
			throw new IllegalStateException("Dados inválidos para a operação.");
		}
		
		this.placeRepository.delete(id);		
	}

	@Override
	public List<String> canCreate(String name, String descripton, int cep, String city, String street, int number, List<Long> categories,
			String email, Long phone) {
		return this.verifyFields(null, name, descripton, cep, city, street, number, categories, email, phone);
	}

	@Override
	public List<String> canUpdate(long id, String name, String descripton, int cep, String city, String street, int number, 
			List<Long> categories,
			String email, Long phone) {
		List<String> errors = new ArrayList<>();
		
		Place one = this.placeRepository.findOne(id);
		if(one == null) {
			errors.add("Local não encontrado.");
		} else {
			errors.addAll(this.verifyFields(id, name, descripton, cep, city, street, number, categories, email, phone));	
		}	
		
		return errors;
	}

	@Override
	public List<String> canDelete(long id) {
		List<String> errors = new ArrayList<>();
		
		Place one = this.placeRepository.findOne(id);
		if(one == null) {
			errors.add("Local não encontrado.");
		}
		
		return errors;
	}


}
