package br.com.turismoinf.web.application.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.turismoinf.web.application.service.IEventService;
import br.com.turismoinf.web.domain.models.event.Event;
import br.com.turismoinf.web.domain.models.event.EventPhoto;
import br.com.turismoinf.web.domain.models.event.IEventRepository;
import br.com.turismoinf.web.utils.CalendarUtil;
import br.com.turismoinf.web.utils.EmailUtil;

@Service
public class EventService implements IEventService {
	
	@Inject private IEventRepository eventRepository;
	
	private List<String> verifyFields(Long id, String name, String description, int cep, String city, String street, int number, 
			String email, Long phone, String dateStart, String dateEnd, String timeStart, String timeEnd, String price) {
		List<String> errors = new ArrayList<>();
		
		if(phone != null && phone.toString().length() != 11) {
			errors.add("Telefone inválido. Informe um telefone junto ao DDD. Ex: 3199887766.");
		}
		
		if(email != null && !EmailUtil.isValidateEmail(email)) {
			errors.add("Email de contato inválido.");
		}
		
		if(StringUtils.isBlank(name)) {
			errors.add("Nome obrigatório.");
		} else if(name.length() > 100) {
			errors.add("Nome máximo de 100 caracteres.");
		} else {
			Event one = this.eventRepository.findByName(name);
			
			if(one != null && !one.getId().equals(id)) {
				errors.add("Nome já existente.");
			}
		}
		
		if(StringUtils.isBlank(description)) {
			errors.add("Descrição obrigatório.");
		} else if(description.length() > 255) {
			errors.add("Descrição máximo de 255 caracteres.");
		}
		
		if(cep == 0) {
			errors.add("Cep obrigatório.");
		} else if(String.valueOf(cep).length() != 8) {
			errors.add("Cep inválido.");
		}
		
		if(StringUtils.isBlank(city)) {
			errors.add("Cidade obrigatório.");
		} else if(description.length() > 255) {
			errors.add("Cidade máximo de 100 caracteres.");
		}
		
		if(StringUtils.isBlank(street)) {
			errors.add("Logradouro obrigatório.");
		} else if(description.length() > 255) {
			errors.add("Logradouro máximo de 255 caracteres.");
		}
		
		if(number == 0) {
			errors.add("Número obrigatório.");
		} else if(String.valueOf(number).length() > 10) {
			errors.add("Número máximo de 10 dígitos.");
		}
		
		Calendar start = CalendarUtil.parseCalendarDateTime("yyyy-MM-dd", dateStart);
		if(start == null) {
			errors.add("Data de início do evento inválida.");
		}
		
		if(dateEnd != null) {
			Calendar end = CalendarUtil.parseCalendarDateTime("yyyy-MM-dd", dateEnd);
			if(end == null) {
				errors.add("Data de início do evento inválida.");
			}
		}
		
		Calendar tStart = CalendarUtil.parseCalendarDateTime("HH:mm", timeStart);
		if(tStart == null) {
			errors.add("Horário de início do evento inválido.");
		}
		
		if(timeEnd != null) {
			Calendar tEnd = CalendarUtil.parseCalendarDateTime("HH:mm", timeEnd);
			if(tEnd == null) {
				errors.add("Horário de fim do evento inválido.");
			}
		}
		
		return errors;
	}

	@Transactional(propagation=Propagation.REQUIRED)
	@Override
	public Event createEvent(String name, String description, int cep, String city, String street, int number, List<String> images,
			String email, Long phone, String dateStart, String dateEnd, String timeStart, String timeEnd, String price) {
		if(!this.canCreate(name, description, cep, city, street, number, email, phone, dateStart, dateEnd, timeStart, timeEnd, price).isEmpty()) {
			throw new IllegalStateException("Dados inválidos para a operação.");
		}
		
		Event event = new Event();
		event.setName(name);
		event.setDescription(description);
		event.setCep(cep);
		event.setCity(city);
		event.setStreet(street);
		event.setNumber(number);
		event.setEmail(email);
		event.setPhone(phone);
		
		Calendar start = CalendarUtil.parseCalendarDateTime("yyyy-MM-dd", dateStart);
		event.setDateStart(start);
		
		if(dateEnd != null) {
			Calendar end = CalendarUtil.parseCalendarDateTime("yyyy-MM-dd", dateEnd);
			event.setDateEnd(end);
		}
		
		Calendar tStart = CalendarUtil.parseCalendarDateTime("HH:mm", timeStart);
		event.setTimeStart(CalendarUtil.getHorarioEmMinutos(tStart));
		
		if(timeEnd != null) {
			Calendar tEnd = CalendarUtil.parseCalendarDateTime("HH:mm", timeEnd);
			event.setTimeEnd(CalendarUtil.getHorarioEmMinutos(tEnd));
		}
		
		if(price != null) {
			event.setPrice(price);
		}
		
		if(images != null && !images.isEmpty()) {
			Set<EventPhoto> photos = new HashSet<>();
			images.forEach(item -> {
				photos.add(new EventPhoto(item, event));
			});
			
			event.setPhotos(photos);
		}
		
		Event newItem = this.eventRepository.save(event);
		
		return newItem;
	}

	@Transactional(propagation=Propagation.REQUIRED)
	@Override
	public void updateEvent(long id, String name, String description, int cep, String city, String street, int number, List<String> images,
			String email, Long phone, String dateStart, String dateEnd, String timeStart, String timeEnd, String price) {
		if(!this.canUpdate(id, name, description, cep, city, street, number, email, phone, dateStart, dateEnd, timeStart, timeEnd, price).isEmpty()) {
			throw new IllegalStateException("Dados inválidos para a operação.");
		}
		
		Event one = this.eventRepository.findOne(id);
		one.setName(name);
		one.setDescription(description);
		one.setCep(cep);
		one.setCity(city);
		one.setStreet(street);
		one.setNumber(number);
		one.setEmail(email);
		one.setPhone(phone);
		
		Calendar start = CalendarUtil.parseCalendarDateTime("yyyy-MM-dd", dateStart);
		one.setDateStart(start);
		
		if(dateEnd != null) {
			Calendar end = CalendarUtil.parseCalendarDateTime("yyyy-MM-dd", dateEnd);
			one.setDateEnd(end);
		}
		
		Calendar tStart = CalendarUtil.parseCalendarDateTime("HH:mm", timeStart);
		one.setTimeStart(CalendarUtil.getHorarioEmMinutos(tStart));
		
		if(timeEnd != null) {
			Calendar tEnd = CalendarUtil.parseCalendarDateTime("HH:mm", timeEnd);
			one.setTimeEnd(CalendarUtil.getHorarioEmMinutos(tEnd));
		}
		
		if(price != null) {
			one.setPrice(price);
		}
		
		this.eventRepository.deletePhotos(id);
		
		if(images != null && !images.isEmpty()) {
			Set<EventPhoto> photos = new HashSet<>();
			images.forEach(item -> {
				photos.add(new EventPhoto(item, one));
			});
			
			one.setPhotos(photos);
		}
		
		this.eventRepository.save(one);		
	}

	@Override
	public void deleteEvent(long id) {
		if(!this.canDelete(id).isEmpty()) {
			throw new IllegalStateException("Dados inválidos para a operação.");
		}
		
		this.eventRepository.delete(id);		
	}

	@Override
	public List<String> canCreate(String name, String descripton, int cep, String city, String street, int number, 
			String email, Long phone, String dateStart, String dateEnd, String timeStart, String timeEnd, String price) {
		return this.verifyFields(null, name, descripton, cep, city, street, number, email, phone, dateStart, dateEnd, timeStart, timeEnd, price);
	}

	@Override
	public List<String> canUpdate(long id, String name, String descripton, int cep, String city, String street, int number, 
			String email, Long phone, String dateStart, String dateEnd, String timeStart, String timeEnd, String price) {
		List<String> errors = new ArrayList<>();
		
		Event one = this.eventRepository.findOne(id);
		if(one == null) {
			errors.add("Evento não encontrado.");
		} else {
			errors.addAll(this.verifyFields(id, name, descripton, cep, city, street, number, email, phone, dateStart, dateEnd, timeStart, timeEnd, price));	
		}	
		
		return errors;
	}

	@Override
	public List<String> canDelete(long id) {
		List<String> errors = new ArrayList<>();
		
		Event one = this.eventRepository.findOne(id);
		if(one == null) {
			errors.add("Evento não encontrado.");
		}
		
		return errors;
	}


}
