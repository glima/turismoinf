package br.com.turismoinf.web.application.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.turismoinf.web.application.service.ICategoryService;
import br.com.turismoinf.web.domain.models.category.Category;
import br.com.turismoinf.web.domain.models.category.ICategoryRepository;

@Service
public class CategoryService implements ICategoryService {

	@Inject private ICategoryRepository categoryRepository;

	private List<String> verifyFields(Long id, String description) {
		List<String> errors = new ArrayList<>();
		
		if(StringUtils.isBlank(description)) {
			errors.add("Descrição obrigatória.");
		} else {
			Category one = this.categoryRepository.findByDescription(description);
			
			if(one != null && !one.getId().equals(id)) {
				errors.add("Descrição já existente.");
			}
		}

		return errors;
	}
	
	@Transactional(propagation=Propagation.REQUIRED)
	@Override
	public Category createCategory(String description, String icon) {
		if(!this.canCreate(description).isEmpty()) {
			throw new IllegalStateException("Dados inválidos para a operação.");
		}
		
		Category category = new Category();
		category.setDescription(description);
		category.setIcon(icon);
		
		Category newItem = this.categoryRepository.save(category);
		
		return newItem;
	}
	
	@Transactional(propagation=Propagation.REQUIRED)
	@Override
	public void updateCategory(long id, String description, String icon) {
		if(!this.canUpdate(id, description).isEmpty()) {
			throw new IllegalStateException("Dados inválidos para a operação.");
		}
		
		Category one = this.categoryRepository.findOne(id);
		one.setDescription(description);
		one.setIcon(icon);
		
		this.categoryRepository.save(one);
	}
	
	@Override
	public void deleteCategory(long id) {
		if(!this.canDelete(id).isEmpty()) {
			throw new IllegalStateException("Dados inválidos para a operação.");
		}
		
		this.categoryRepository.delete(id);
	}
	
	@Override
	public List<String> canCreate(String description) {
		return this.verifyFields(null, description);
	}
	
	@Override
	public List<String> canUpdate(long id, String description) {
		List<String> errors = new ArrayList<>();
		
		Category one = this.categoryRepository.findOne(id);
		if(one == null) {
			errors.add("Categoria não encontrada.");
		} else {
			errors.addAll(this.verifyFields(id, description));	
		}	
		
		return errors;
	}
	
	@Override
	public List<String> canDelete(long id) {
		List<String> errors = new ArrayList<>();
		
		Category one = this.categoryRepository.findOne(id);
		if(one == null) {
			errors.add("Categoria não encontrada.");
		}
		
		return errors;
	}


}
