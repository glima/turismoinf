package br.com.turismoinf.web;

import javax.annotation.Resource;
import javax.inject.Inject;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

@Configuration
@EnableWebSecurity
@ComponentScan(value = {
	"br.com.turismoinf.web"
})
@Order(1)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	private @Inject @Resource(name = "userDao") UserDetailsService users;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	http
		.antMatcher("/ui/**")
		.authorizeRequests()
		.antMatchers("/ui/**").authenticated()
		.anyRequest().authenticated()
		.and().
			formLogin()
			.defaultSuccessUrl("/ui", true)
			.loginProcessingUrl("/ui/login")
			.failureUrl( "/ui/login.html?erro=1" )
			.loginPage("/ui/login.html").permitAll()
		.and()
		.logout()
		.logoutUrl("/ui/logout")
        .logoutSuccessUrl("/ui/login.html")
        .deleteCookies("JSESSIONID")
        .and()
        .csrf().disable();
    }
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(users).passwordEncoder(new Md5PasswordEncoder());
	}
	
}
