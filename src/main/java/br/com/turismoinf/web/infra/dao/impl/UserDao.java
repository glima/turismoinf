package br.com.turismoinf.web.infra.dao.impl;

import org.hibernate.criterion.Restrictions;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import br.com.turismoinf.web.domain.models.user.User;

@Repository("userDao")
public class UserDao extends Dao implements UserDetailsService {

	@Override
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		User user = (User) getSession().createCriteria(User.class)
			.add(Restrictions.eq("login", login))
			.uniqueResult();
			
			if(user == null) {
				throw new UsernameNotFoundException
					("O usuario de login " + login + " não existe");
			}
			
			return user;
	}

}
