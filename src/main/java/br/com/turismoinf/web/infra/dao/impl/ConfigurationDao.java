package br.com.turismoinf.web.infra.dao.impl;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import br.com.turismoinf.web.interfaceapi.dto.ConfigurationDto;

@Repository("confDao")
public class ConfigurationDao extends Dao {
	
	public ConfigurationDto getConfiguration() {
		StringBuilder query = new StringBuilder();
		query.append("select chave, valor");
		query.append(" from configuracao c");
		
		SQLQuery sqlQuery = getSession().createSQLQuery(query.toString());
		@SuppressWarnings("unchecked")
		List<Object[]> objects = sqlQuery.list();
		
		ConfigurationDto dto = new ConfigurationDto();
		for (Object[] obj : objects) {
			
			switch (obj[0].toString()) {
			case "email_servidor":
				dto.setServer(obj[1] == null ? null : obj[1].toString());
				break;
				
			case "email_conta":
				dto.setEmail(obj[1] == null ? null : obj[1].toString());
				break;
				
			case "email_porta":
				dto.setPort(obj[1] == null ? null : Integer.valueOf(obj[1].toString()));
				break;
				
			case "email_senha":
				dto.setPassword(obj[1] == null ? null : obj[1].toString());
				break;
				
			case "telefone_contato":
				dto.setPhoneContact(obj[1] == null ? null : Long.valueOf(obj[1].toString()));
				break;
				
			case "email_contato":
				dto.setEmailContact(obj[1] == null ? null : obj[1].toString());
				break;
				
			case "descricao_inicial":
				dto.setDescription(obj[1] == null ? null : obj[1].toString());
				break;
				
			case "imagem_inicial":
				dto.setImage(obj[1] == null ? null : obj[1].toString());
				break;

			default:
				break;
			}
		}
		
		return dto;
	}

	public void updateEmailConfiguration(Map<String, String> conf) {
		boolean isEmailConf = this.thereIsEmailConf();
		
		if(isEmailConf) {
			this.updateEmailConf(conf);
		} else {
			this.createEmailConf(conf);
		}

		boolean isContactConf = this.thereIsContactConf();
		
		if(isContactConf) {
			this.updateContactConf(conf);
		} else {
			this.createContactConf(conf);
		}
		
		boolean isInitialConf = this.thereIsInitialConf();
		
		if(isInitialConf) {
			this.updateInitialConf(conf);
		} else {
			this.createInitialConf(conf);
		}
	}
	
	public boolean thereIsEmailConf() {
		StringBuilder query = new StringBuilder();
		query.append("select count(chave) total");
		query.append(" from configuracao");
		query.append(" where chave in ('email_servidor') ");

		SQLQuery sqlQuery = getSession().createSQLQuery(query.toString());
		
		BigInteger total = (BigInteger) sqlQuery.uniqueResult();
		
		return total.intValue() > 0;
	}
	
	private void updateEmailConf(Map<String, String> conf) {
		StringBuilder query = new StringBuilder();
		query.append("update configuracao");
		query.append(" set valor = :value ");
		query.append(" where chave = :key");
		
		SQLQuery sqlQuery = getSession().createSQLQuery(query.toString());
		sqlQuery.setParameter("value", conf.get("email_servidor") != null ? conf.get("email_servidor").toString() : null);
		sqlQuery.setParameter("key", "email_servidor");
		sqlQuery.executeUpdate();
		
		sqlQuery = getSession().createSQLQuery(query.toString());
		sqlQuery.setParameter("value", conf.get("email_conta") != null ? conf.get("email_conta").toString() : null);
		sqlQuery.setParameter("key", "email_conta");
		sqlQuery.executeUpdate();
		
		sqlQuery = getSession().createSQLQuery(query.toString());
		sqlQuery.setParameter("value", conf.get("email_porta") != null ? conf.get("email_porta").toString() : null);
		sqlQuery.setParameter("key", "email_porta");
		sqlQuery.executeUpdate();
		
		sqlQuery = getSession().createSQLQuery(query.toString());
		sqlQuery.setParameter("value", conf.get("email_senha") != null ? conf.get("email_senha").toString() : null);
		sqlQuery.setParameter("key", "email_senha");
		sqlQuery.executeUpdate();
	}
	
	private void createEmailConf(Map<String, String> conf) {
		StringBuilder query = new StringBuilder();
		query.append("insert into configuracao(chave, valor) values ");
		query.append(" ('email_servidor', :value1), ");
		query.append(" ('email_conta', :value2), ");
		query.append(" ('email_porta', :value3), ");
		query.append(" ('email_senha', :value4) ");
		
		SQLQuery sqlQuery = getSession().createSQLQuery(query.toString());
		sqlQuery.setParameter("value1", conf.get("email_servidor") != null ? conf.get("email_servidor").toString() : null);
		sqlQuery.setParameter("value2", conf.get("email_conta") != null ? conf.get("email_conta").toString() : null);
		sqlQuery.setParameter("value3", conf.get("email_porta") != null ? conf.get("email_porta").toString() : null);
		sqlQuery.setParameter("value4", conf.get("email_senha") != null ? conf.get("email_senha").toString() : null);
		sqlQuery.executeUpdate();
	}
	
	private boolean thereIsContactConf() {
		StringBuilder query = new StringBuilder();
		query.append("select count(chave) total");
		query.append(" from configuracao");
		query.append(" where chave in ('telefone_contato', 'email_contato') ");

		SQLQuery sqlQuery = getSession().createSQLQuery(query.toString());
		
		BigInteger total = (BigInteger) sqlQuery.uniqueResult();
		
		return total.intValue() > 0;
	}
	
	private void updateContactConf(Map<String, String> conf) {
		StringBuilder query = new StringBuilder();
		query.append("update configuracao");
		query.append(" set valor = :value ");
		query.append(" where chave = :key");
		
		SQLQuery sqlQuery = getSession().createSQLQuery(query.toString());
		sqlQuery.setParameter("value", conf.get("telefone_contato") != null ? conf.get("telefone_contato").toString() : null);
		sqlQuery.setParameter("key", "telefone_contato");
		sqlQuery.executeUpdate();
		
		sqlQuery = getSession().createSQLQuery(query.toString());
		sqlQuery.setParameter("value", conf.get("email_contato") != null ? conf.get("email_contato").toString() : null);
		sqlQuery.setParameter("key", "email_contato");
		sqlQuery.executeUpdate();
	}
	
	private void createContactConf(Map<String, String> conf) {
		StringBuilder query = new StringBuilder();
		query.append("insert into configuracao(chave, valor) values ");
		query.append(" ('telefone_contato', :value1), ");
		query.append(" ('email_contato', :value2) ");
		
		SQLQuery sqlQuery = getSession().createSQLQuery(query.toString());
		sqlQuery.setParameter("value1", conf.get("telefone_contato") != null ? conf.get("telefone_contato").toString() : null);
		sqlQuery.setParameter("value2", conf.get("telefone_contato") != null ? conf.get("email_contato").toString() : null);
		sqlQuery.executeUpdate();
	}
	
	private boolean thereIsInitialConf() {
		StringBuilder query = new StringBuilder();
		query.append("select count(chave) total");
		query.append(" from configuracao");
		query.append(" where chave in ('descricao_inicial', 'imagem_inicial') ");

		SQLQuery sqlQuery = getSession().createSQLQuery(query.toString());
		
		BigInteger total = (BigInteger) sqlQuery.uniqueResult();
		
		return total.intValue() > 0;
	}
	
	private void updateInitialConf(Map<String, String> conf) {
		StringBuilder query = new StringBuilder();
		query.append("update configuracao");
		query.append(" set valor = :value ");
		query.append(" where chave = :key");
		
		SQLQuery sqlQuery = getSession().createSQLQuery(query.toString());
		sqlQuery.setParameter("value", conf.get("imagem_inicial") != null ? conf.get("imagem_inicial").toString() : null);
		sqlQuery.setParameter("key", "imagem_inicial");
		sqlQuery.executeUpdate();
		
		sqlQuery = getSession().createSQLQuery(query.toString());
		sqlQuery.setParameter("value", conf.get("descricao_inicial") != null ? conf.get("descricao_inicial").toString() : null);
		sqlQuery.setParameter("key", "descricao_inicial");
		sqlQuery.executeUpdate();
	}
	
	private void createInitialConf(Map<String, String> conf) {
		StringBuilder query = new StringBuilder();
		query.append("insert into configuracao(chave, valor) values ");
		query.append(" ('imagem_inicial', :value1), ");
		query.append(" ('descricao', :value2) ");
		
		SQLQuery sqlQuery = getSession().createSQLQuery(query.toString());
		sqlQuery.setParameter("value1", conf.get("imagem_inicial") != null ? conf.get("imagem_inicial").toString() : null);
		sqlQuery.setParameter("value2", conf.get("descricao_inicial") != null ? conf.get("descricao").toString() : null);
		sqlQuery.executeUpdate();
	}
}
