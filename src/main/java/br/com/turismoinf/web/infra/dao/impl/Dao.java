package br.com.turismoinf.web.infra.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;

public abstract class Dao {

	@PersistenceContext
	private EntityManager entityManager;
	
	public Session getSession(){
		if(this.entityManager != null){
			Session session = (Session)this.entityManager.getDelegate();			
			if(session.isOpen() == false){
				return session.getSessionFactory().openSession();
			}else{
				return session;
			}
		}
		return null;
	}

	public EntityManager getEntityManager(){
		return this.entityManager;
	}
	
	public int getPage(int page, int limit) {
		if(page <= 0) {
			return 0;
		}

		return (page) * limit;
	}
}
