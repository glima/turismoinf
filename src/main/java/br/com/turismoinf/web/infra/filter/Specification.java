package br.com.turismoinf.web.infra.filter;

import java.util.List;

import org.hibernate.SQLQuery;

public class Specification {

	private List<SearchCriteria> criterias;
	private StringBuilder builder;
	
	public Specification(List<SearchCriteria> criterias, StringBuilder builder) {
		this.criterias = criterias;
		this.builder = builder;
	}

	public SQLQuery defineParameters(SQLQuery query) {
		for (SearchCriteria criteria : criterias) {
			if(criteria.getOperation().equals(SearchOperation.LIKE)) {
				query.setParameter(criteria.getKey(), "%" + criteria.getValue() + "%");
			} else {
				query.setParameter(criteria.getKey(), criteria.getValue());
			}
		}
		
		return query;
	}
	
	public StringBuilder getClause() {
		return this.builder;
	}
}
