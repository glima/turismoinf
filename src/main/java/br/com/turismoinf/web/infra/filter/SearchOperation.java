package br.com.turismoinf.web.infra.filter;

public enum SearchOperation {

	EQUALITY, GREATER_THAN, LESS_THAN, LIKE;
	 
    public static final String[] SIMPLE_OPERATION_SET = { ":", ">", "<", "~" };
 
    public static SearchOperation getSimpleOperation(char input) {
        switch (input) {
        case ':':
            return EQUALITY;
        case '>':
            return GREATER_THAN;
        case '<':
            return LESS_THAN;
        case '~':
            return LIKE;
        default:
            return null;
        }
    }
}
