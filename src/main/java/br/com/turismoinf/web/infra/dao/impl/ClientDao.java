package br.com.turismoinf.web.infra.dao.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import br.com.turismoinf.web.interfaceapi.dto.CategoryDto;
import br.com.turismoinf.web.interfaceapi.dto.EventDto;
import br.com.turismoinf.web.interfaceapi.dto.ListDto;
import br.com.turismoinf.web.interfaceapi.dto.PlaceDto;
import br.com.turismoinf.web.utils.CalendarUtil;

@Repository("clientDao")
public class ClientDao extends Dao {

	private static int TOTAL = 10;

	public String getCategoryImage(long id) {
		StringBuilder query = new StringBuilder();
		query.append("select icone from categoria where id = :id");
		
		SQLQuery qry = getSession().createSQLQuery(query.toString());
		qry.setParameter("id", id);
		
		Object uniqueResult = qry.uniqueResult();
		if(uniqueResult != null) {
			return qry.uniqueResult().toString();
		} else {
			return null;
		}
	}
	
	public String getPlaceImage(long id) {
		StringBuilder query = new StringBuilder();
		query.append("select imagem from fotos_local where id = :id");
		
		SQLQuery qry = getSession().createSQLQuery(query.toString());
		qry.setParameter("id", id);
		
		String img = qry.uniqueResult().toString();
		
		return img;
	}
	
	public List<CategoryDto> listCategories(int page) {
		StringBuilder query = new StringBuilder();
		query.append("select c.id, c.descricao ");
		query.append(" from categoria c ");
		query.append(" order by c.descricao");
		query.append(" limit :limit offset :offset");
		
		SQLQuery qry = getSession().createSQLQuery(query.toString());
		qry.setParameter("limit", TOTAL);
		qry.setParameter("offset", getPage(page, TOTAL));
		
		List<Object[]> objects = qry.list();
		getSession().close();
		List<CategoryDto> list = new ArrayList<>();
		
		for (Object[] obj : objects) {
			CategoryDto dto = new CategoryDto();
			
			dto.setId((int) obj[0]);
			dto.setDescription(obj[1].toString());
			
			list.add(dto);
		}
		
		return list;
	}
	
	public Map<Long, List<ListDto>> listCategoriesPlaces(List<Long> places) {
		StringBuilder query = new StringBuilder();
		query.append("select c.id, c.descricao, lc.local_id ");
		query.append(" from categoria c ");
		query.append(" join local_categoria lc on lc.categoria_id = c.id");
		query.append(" where lc.local_id in (:places)");
		query.append(" order by c.descricao");
		
		SQLQuery qry = getSession().createSQLQuery(query.toString());
		qry.setParameterList("places", places);
		
		List<Object[]> objects = qry.list();
		Map<Long, List<ListDto>> list = new HashMap<>();
		
		for (Object[] obj : objects) {
			ListDto dto = new ListDto();
			
			dto.setId((int) obj[0]);
			dto.setDescription(obj[1].toString());
			
			if(list.containsKey(new Long((Integer) obj[2]))) {
				list.get(new Long((Integer) obj[2])).add(dto);
			} else {
				List<ListDto> array = new ArrayList<>();
				array.add(dto);
				list.put(new Long((Integer) obj[2]), array);
			}			
		}
		
		return list;
	}
	
	public Map<Long, List<String>> listPhotosEvents(List<Long> events) {
		StringBuilder query = new StringBuilder();
		query.append("select e.id, fe.id idFe ");
		query.append(" from fotos_evento fe ");
		query.append(" join evento e on e.id = fe.evento_id");
		query.append(" where e.id in (:events)");
		
		SQLQuery qry = getSession().createSQLQuery(query.toString());
		qry.setParameterList("events", events);
		
		List<Object[]> objects = qry.list();
		Map<Long, List<String>> list = new HashMap<>();
		
		for (Object[] obj : objects) {
			if(list.containsKey(new Long((Integer) obj[0]))) {
				list.get(new Long((Integer) obj[0])).add(obj[1].toString());
			} else {
				List<String> array = new ArrayList<>();
				array.add(obj[1].toString());
				list.put(new Long((Integer) obj[0]), array);
			}			
		}
		
		return list;
	}
	
	public Map<Long, List<String>> listPhotosPlaces(List<Long> places) {
		StringBuilder query = new StringBuilder();
		query.append("select l.id, fl.id idFl ");
		query.append(" from fotos_local fl ");
		query.append(" join local l on l.id = fl.local_id");
		query.append(" where l.id in (:places)");
		
		SQLQuery qry = getSession().createSQLQuery(query.toString());
		qry.setParameterList("places", places);
		
		List<Object[]> objects = qry.list();
		Map<Long, List<String>> list = new HashMap<>();
		
		for (Object[] obj : objects) {
			if(list.containsKey(new Long((Integer) obj[0]))) {
				list.get(new Long((Integer) obj[0])).add(obj[1].toString());
			} else {
				List<String> array = new ArrayList<>();
				array.add(obj[1].toString());
				list.put(new Long((Integer) obj[0]), array);
			}			
		}
		
		return list;
	}
	
	public List<PlaceDto> listPlaces(long idCategory, int page) {
		StringBuilder query = new StringBuilder();
		query.append("select l.id, l.nome, l.descricao, l.cep, l.cidade, l.rua, l.numero, l.email, l.telefone");
		query.append(" from local l");
		query.append(" join local_categoria lc on lc.local_id = l.id");
		query.append(" where lc.categoria_id = :category");
		query.append(" order by l.nome");
		query.append(" limit :limit offset :offset");
		
		SQLQuery qry = getSession().createSQLQuery(query.toString());
		qry.setParameter("category", idCategory);
		qry.setParameter("limit", TOTAL);
		qry.setParameter("offset", getPage(page, TOTAL));
		
		List<Object[]> objects = qry.list();
		getSession().close();
		List<PlaceDto> list = new ArrayList<>();
		
		for (Object[] obj : objects) {
			PlaceDto dto = new PlaceDto();
			
			dto.setId((int) obj[0]);
			dto.setName(obj[1].toString());
			dto.setDescription(obj[2].toString());
			dto.setCep((int) obj[3]);
			dto.setCity(obj[4].toString());
			dto.setStreet(obj[5].toString());
			dto.setNumber(Integer.valueOf(obj[6].toString()));
			dto.setEmail(obj[7] == null ? null : obj[7].toString());
			dto.setPhone(obj[8] == null ? null : Long.valueOf(obj[8].toString()));
			
			list.add(dto);
		}
		
		List<Long> idsPlaces = new ArrayList<>();
		list.forEach(item -> idsPlaces.add(item.getId()));
		
		if(!idsPlaces.isEmpty()) {
			Map<Long, List<ListDto>> categoriesPlaces = this.listCategoriesPlaces(idsPlaces);
			Map<Long, List<String>> photosPlaces = this.listPhotosPlaces(idsPlaces);
			
			list.forEach(item -> {
				item.setCategories(categoriesPlaces.get(item.getId()));
				item.setImages(photosPlaces.get(item.getId()));
			});
		}
		
		return list;
	}
	
	public List<String> listPhotos(long idPlace) {
		StringBuilder query = new StringBuilder();
		query.append("select imagem");
		query.append(" from fotos_local fl");
		query.append(" where local_id = :place");

		SQLQuery qry = getSession().createSQLQuery(query.toString());
		qry.setParameter("place", idPlace);
		
		List<Object[]> objects = qry.list();
		List<String> list = new ArrayList<>();
		
		for (Object[] obj : objects) {
			list.add(obj[0].toString());
		}
		
		return list;
	}

	public String getEventImage(long id) {
		StringBuilder query = new StringBuilder();
		query.append("select imagem from fotos_evento where id = :id");
		
		SQLQuery qry = getSession().createSQLQuery(query.toString());
		qry.setParameter("id", id);
		
		String img = qry.uniqueResult().toString();
		
		return img;
	}

	public List<EventDto> listEvents(int page) {
		StringBuilder query = new StringBuilder();
		query.append("select e.id, e.nome, e.descricao, e.cep, e.cidade, e.rua, e.numero, e.email, e.telefone, e.data_inicio, e.data_fim,");
		query.append(" e.horario_inicio, e.horario_fim, e.valor");
		query.append(" from evento e");
		query.append(" where e.data_inicio >= :dateStart");
		query.append(" order by e.nome");
		query.append(" limit :limit offset :offset");
		
		SQLQuery qry = getSession().createSQLQuery(query.toString());
		qry.setParameter("dateStart", Calendar.getInstance());
		qry.setParameter("limit", TOTAL);
		qry.setParameter("offset", getPage(page, TOTAL));
		
		List<Object[]> objects = qry.list();
		getSession().close();
		List<EventDto> list = new ArrayList<>();
		
		for (Object[] obj : objects) {
			EventDto dto = new EventDto();
			
			dto.setId((int) obj[0]);
			dto.setName(obj[1].toString());
			dto.setDescription(obj[2].toString());
			dto.setCep((int) obj[3]);
			dto.setCity(obj[4].toString());
			dto.setStreet(obj[5].toString());
			dto.setNumber(Integer.valueOf(obj[6].toString()));
			dto.setEmail(obj[7] == null ? null : obj[7].toString());
			dto.setPhone(obj[8] == null ? null : Long.valueOf(obj[8].toString()));
			dto.setDateStart(CalendarUtil.formata("dd/MM/yyyy", CalendarUtil.parseCalendarDate(obj[9].toString())));
			dto.setDateEnd(obj[10] == null ? null : CalendarUtil.formata("dd/MM/yyyy", CalendarUtil.parseCalendarDate(obj[10].toString())));
			dto.setTimeStart(CalendarUtil.formata("HH:mm", CalendarUtil.minutosParaCalendar((int) obj[11])));
			dto.setTimeEnd(obj[12] == null ? null : CalendarUtil.formata("HH:mm", CalendarUtil.minutosParaCalendar((int) obj[12])));
			dto.setPrice(obj[13] == null ? "Gratuito" : obj[13].toString());
			
			list.add(dto);
		}
		
		List<Long> idsEvents = new ArrayList<>();
		list.forEach(item -> idsEvents.add(item.getId()));
		
		if(!idsEvents.isEmpty()) {
			Map<Long, List<String>> photosPlaces = this.listPhotosEvents(idsEvents);
			
			list.forEach(item -> {
				item.setImages(photosPlaces.get(item.getId()));
			});
		}
		
		return list;
	}
}
