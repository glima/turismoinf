package br.com.turismoinf.web.infra.filter;

public class SpecField {
	
	public enum FieldType {
		DATE, NUMBER, TEXT
	}

	private FieldType type;
	private String field;
	
	public SpecField(FieldType type, String field) {
		this.type = type;
		this.field = field;
	}
	
	public boolean isDate() {
		if(type.equals(FieldType.DATE)) {
			return true;
		}
		
		return false;
	}
	
	public boolean isNumber() {
		if(type.equals(FieldType.NUMBER)) {
			return true;
		}
		
		return false;
	}
	
	public boolean isText() {
		if(type.equals(FieldType.TEXT)) {
			return true;
		}
		
		return false;
	}
	
	public String getField() {
		return field;
	}
}
