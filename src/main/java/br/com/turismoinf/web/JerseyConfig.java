package br.com.turismoinf.web;

import java.util.Set;
import java.util.regex.Pattern;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.spring.scope.RequestContextFilter;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.type.filter.RegexPatternTypeFilter;
import org.springframework.util.ClassUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

@Configuration
@ApplicationPath("/api")
public class JerseyConfig extends ResourceConfig {

	public JerseyConfig(ObjectMapper objectMapper) {
		register(RequestContextFilter.class);
		register(new ObjectMapperContextResolver(objectMapper));
		register(MultiPartFeature.class);
		
		ClassPathScanningCandidateComponentProvider provider = new ClassPathScanningCandidateComponentProvider(false);
	    provider.addIncludeFilter(new RegexPatternTypeFilter(Pattern.compile(".*")));
	    
	    Set<BeanDefinition> candidateComponents = provider.findCandidateComponents("br.com.turismoinf.web.interfaceapi.controller");
	    candidateComponents.addAll(provider.findCandidateComponents("br.com.turismoinf.web.interfaceapi.jersey"));
	    for (BeanDefinition candidateComponent : candidateComponents) {
	      register(ClassUtils.resolveClassName(candidateComponent.getBeanClassName(), ClassUtils.getDefaultClassLoader()));
	    }
	}
	
	@Provider
	public static class ObjectMapperContextResolver implements ContextResolver<ObjectMapper> {
	 
		private final ObjectMapper mapper;
	 
	    public ObjectMapperContextResolver(ObjectMapper mapper) {
	    	mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
	    	this.mapper = mapper;
	    }
	 
	    public ObjectMapper getContext(Class<?> type) {
	    	return mapper;
	    }
	}
}
