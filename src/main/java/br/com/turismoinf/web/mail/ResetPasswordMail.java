package br.com.turismoinf.web.mail;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class ResetPasswordMail implements TemplateMail {

	public static final String TEMPLATE = "templates/template_reset_password.ftl";
	
	public static final String USERNAME = "username";
	public static final String NEW_PASS = "newpass";
	
	public String adicionar(Map<String, Object> conteudo) {
		String textComum = conteudo.get(USERNAME) + "<br><br>Nova Senha: " + conteudo.get(NEW_PASS) + "<br>";
		@SuppressWarnings("deprecation")
		Configuration conf = new Configuration();
		
		try {
			conf.setTemplateLoader(new ClassTemplateLoader(getClass(), "/"));
			Template template = conf.getTemplate(TEMPLATE);
			Map<String, Object> rootMap = new HashMap<String, Object>();
			
            rootMap.put( USERNAME, conteudo.get(USERNAME));
            rootMap.put( NEW_PASS, conteudo.get(NEW_PASS));
            
            Writer out = new StringWriter();
            template.process(rootMap, out);
			
            return out.toString();
		} catch (IOException e) {
			e.printStackTrace();
			return textComum;
		} catch (TemplateException e) {
			e.printStackTrace();
			return textComum;
		}
	}
}
