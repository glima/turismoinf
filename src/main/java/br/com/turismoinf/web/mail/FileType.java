package br.com.turismoinf.web.mail;

public enum FileType{

	/*											Constantes
	 * ------------------------------------------------------------------------
	 */
	
	/** mime type <b>Joint Photographic Experts Group</b>.  */
	JPG("jpg", "image/jpeg"),
	
	/** mime type <b>Portable Document Format</b>.  */
	PDF("pdf", "application/pdf"),
	
	/** mime type <b>Portable Network Graphics</b>.  */
	PNG("png", "image/png"),
	
	/** mime type <b>formato de compacta��o de arquivos</b>.  */
	ZIP("zip", "application/zip");
	
	/*											Atributos
	 * -------------------------------------------------------------------------
	 */

	private final String extensao;
	private final String mimeType;
	
	/*											Construtores
	 * -------------------------------------------------------------------------
	 */

	/** 
	 * @param extensao
	 * @param mimeType
	 */
	private FileType(String extensao, String mimeType){
		this.extensao = extensao;
		this.mimeType = mimeType;
	}

	/*											Override's
	 * -------------------------------------------------------------------------
	 */
	
	/**
	 * @return A extens�o do tipo de arquivo.
	 */
	public String getExtensao(){
		return extensao;
	}
	
	/**
	 * @return O mime type do tipo de arquivo.
	 */
	public String getMimeType(){
		return mimeType;
	}
	
	/*											Override's
	 * -------------------------------------------------------------------------
	 */

	/**
	 * @return A extens�o do tipo de arquivo.
	 */
	@Override
	public String toString(){
		return this.extensao;
	}
}
