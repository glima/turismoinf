package br.com.turismoinf.web.mail;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class SendMail implements Serializable {	

	/*	
	 * Constantes
	 ***********************/
	private static final long serialVersionUID = -8289156535979727477L;
	private static final String MIME_CHARSET = "ISO-8859-1";
	private static final boolean DEBUGGING = false;
	

	/*	
	 * Atributos
	 ***********************/
	private Logger logger = Logger.getLogger(SendMail.class.getName());
	
	private InternetAddress user;
	private String password;
	private String host;
	private int port;	
	private boolean ssl;
	private boolean auth;
	private TemplateMail template;
	

	/*	
	 * Construtores
	 ***********************/
	/**
	 * Construtor padr�o para o enviador de e-mails.
	 * 
	 * @param configMail - configura��o do email para envio de mensagens
	 *  
	 * 	- usuario Usu�rio responsável pelo envio de email. Exemplo: new InternetAddress("fulano@gmail.com", "Fulando de Tal")
	 * 	- senha Senha do usuário respons�vel pelo envio de email. Exemplo: "FdT1234"
	 * 	- servidor Host do servidor de envio de e-mails. Exemplo: smtp.gmail.com
	 * 	- porta Porta do servidor de emails. Exemplo: 465
	 * 	- ssl Este servidor requer ssl. Exemplo: true
	 */
	public SendMail(ConfigMail configMail){
		super();
		this.user = configMail.getUsuario();
		this.password = configMail.getSenha();
		this.host = configMail.getServidor();
		this.port = configMail.getPorta();
		this.ssl = configMail.isSsl();
		this.auth = true;
	}
	

	/*	
	 * M�todos
	 ***********************/
	/**
	 * M�todo respons�vel por enviar o e-mail.
	 *  
	 * @param subject Assunto do e-mail. 
	 * @param text Conteudo que ser� enviado por e-mail.
	 * @param to Destinat�rios do e-mail.
	 * 
	 * @throws MessagingException
	 * @throws UnsupportedEncodingException
	 */
	public void send(String subject, String text, Collection<Address> to) throws UnsupportedEncodingException, MessagingException{
		this.send(subject, text, to, null, null);
	}
	
	public void send(String subject, Map<String, Object> text, Collection<Address> to, TemplateMail template) throws UnsupportedEncodingException, MessagingException{
		this.send(subject, text, to, null, template);
	}
	
	/**
	 * M�todo respons�vel por enviar o e-mail.
	 * 
	 * @param subject Assunto do e-mail. 
	 * @param text Conteudo que ser� enviado por e-mail.
	 * @param to Destinat�rios do e-mail.
	 * @param attachments arquivos que iram em anexo no e-mail.
	 * 
	 * @throws MessagingException
	 * @throws UnsupportedEncodingException
	 */
	public void send(String subject, Map<String, Object> text, Collection<Address> to, Collection<MailAttachment> attachments, TemplateMail template) throws UnsupportedEncodingException, MessagingException{
		this.send(subject, text, to, null, null, attachments, template);
	}
	
	/**
	 * M�todo respons�vel por enviar o e-mail.
	 * 
	 * @param subject Assunto do e-mail. 
	 * @param text Conteudo que ser� enviado por e-mail.
	 * @param to Destinat�rios do e-mail.
	 * @param cc Destinat�rios que ser�o enviados como copia o e-mail. 
	 * @param bcc  Destinat�rios que ser�o enviados como copia oculta o e-mail. 
	 * 
	 * @throws MessagingException
	 * @throws UnsupportedEncodingException
	 */
	public void send(String subject, String text, Collection<Address> to, Collection<Address> cc, Collection<Address> bcc) throws MessagingException, UnsupportedEncodingException{
		this.send(subject, text, to, null, null, null);
	}
	
	/**
	 * M�todo respons�vel por enviar o e-mail com um template para a mensagem.
	 * 
	 * @param subject Assunto do e-mail. 
	 * @param text Conteudo que ser� enviado por e-mail.
	 * @param to Destinat�rios do e-mail.
	 * @param cc Destinat�rios que ser�o enviados como copia o e-mail. 
	 * @param bcc  Destinat�rios que ser�o enviados como copia oculta o e-mail. 
	 * @param attachments arquivos que iram em anexo no e-mail.
	 * @param template template utilizado para formata��o da mensagem
	 * 
	 * @throws MessagingException
	 * @throws UnsupportedEncodingException
	 */
	public void send(String subject, Map<String, Object> text, Collection<Address> to, Collection<Address> cc, Collection<Address> bcc, Collection<MailAttachment> attachments, TemplateMail template) throws MessagingException, UnsupportedEncodingException{
		this.template = template;
		
		logger.info("Enviando e-mail.");
		
		MimeMessage message = new MimeMessage(this.getSession());
		message.setFrom(this.user);
		message.setSubject(subject);
		
		if(to != null && to.isEmpty() == false){
			message.setRecipients(Message.RecipientType.TO, (Address[]) to.toArray(new Address[to.size()]));
		}
		
		if(cc != null && cc.isEmpty() == false){
			message.setRecipients(Message.RecipientType.CC, (Address[]) cc.toArray(new Address[cc.size()]));
		}
		
		if(bcc != null && bcc.isEmpty() == false){
			message.setRecipients(Message.RecipientType.BCC, (Address[]) bcc.toArray(new Address[bcc.size()]));
		}
		
		Multipart messageContent = this.buildMessageContent(text);
		
		adicionarAttachments(attachments, messageContent);
		
		message.setContent(messageContent);
		Transport.send(message);	
		logger.info("E-mail enviado com sucesso!");
	}
	
	/**
	 * M�todo respons�vel por enviar o e-mail.
	 * 
	 * @param subject Assunto do e-mail. 
	 * @param text Conteudo que ser� enviado por e-mail.
	 * @param to Destinat�rios do e-mail.
	 * @param cc Destinat�rios que ser�o enviados como copia o e-mail. 
	 * @param bcc  Destinat�rios que ser�o enviados como copia oculta o e-mail. 
	 * @param attachments arquivos que iram em anexo no e-mail.
	 * 
	 * @throws MessagingException
	 * @throws UnsupportedEncodingException
	 */
	public void send(String subject, String text, Collection<Address> to, Collection<Address> cc, Collection<Address> bcc, Collection<MailAttachment> attachments) throws MessagingException, UnsupportedEncodingException{
		logger.info("Enviando e-mail.");
		
		MimeMessage message = new MimeMessage(this.getSession());
		message.setFrom(this.user);
		message.setSubject(subject);
		
		if(to != null && to.isEmpty() == false){
			message.setRecipients(Message.RecipientType.TO, (Address[]) to.toArray(new Address[to.size()]));
		}
		
		if(cc != null && cc.isEmpty() == false){
			message.setRecipients(Message.RecipientType.CC, (Address[]) cc.toArray(new Address[cc.size()]));
		}
		
		if(bcc != null && bcc.isEmpty() == false){
			message.setRecipients(Message.RecipientType.BCC, (Address[]) bcc.toArray(new Address[bcc.size()]));
		}
		
		Multipart messageContent = this.buildMessageContent(text);
		
		adicionarAttachments(attachments, messageContent);
		
		message.setContent(messageContent);
		Transport.send(message);	
		logger.info("E-mail enviado com sucesso!");
	}

	/**
	 * M�todo respons�vel por adicionar os anexos ao corpo do e-mail.
	 * 
	 * @param attachments Anexos a serem adicionados ao e-mail.
	 */
	private void adicionarAttachments(Collection<MailAttachment> attachments,
			Multipart messageContent) throws MessagingException{
		
		if(attachments != null && attachments.isEmpty() == false){
			logger.info("Adicionando anexos ao e-mail.");
			for(MailAttachment attach : attachments){
				messageContent.addBodyPart(attach.getMimeBodyPart());
			}
		}
	}
	
	/**
	 * M�todo respons�vel por construir o corpo do e-mail contendo o texto a ser inserido utilizando um template.
	 * 
	 * @param text Conteudo que ser� enviado por e-mail.
	 * 
	 * @throws UnsupportedEncodingException
	 * @throws MessagingException 
	 * 
	 * @return corpo do texto a ser enviado construido.
	 */
	private Multipart buildMessageContent(Map<String, Object> text) throws UnsupportedEncodingException, MessagingException{
		logger.info("Adicionando corpo de texto.");
		MimeBodyPart content = new MimeBodyPart();
		
		content.setText(this.addTemplate(text), MIME_CHARSET);
		content.setHeader("Content-Type", "text/html; charset=" + MIME_CHARSET);
		content.addHeader("Content-Transfer-Encoding", "base64");
		
		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(content);
		return multipart;
	}
	
	/**
	 * M�todo respons�vel por construir o corpo do e-mail contendo o texto a ser inserido.
	 * 
	 * @param text Conteudo que ser� enviado por e-mail.
	 * 
	 * @throws UnsupportedEncodingException
	 * @throws MessagingException 
	 * 
	 * @return corpo do texto a ser enviado construido.
	 */
	private Multipart buildMessageContent(String text) throws UnsupportedEncodingException, MessagingException{
		logger.info("Adicionando corpo de texto.");
		MimeBodyPart content = new MimeBodyPart();
		
		content.setText(text, MIME_CHARSET);
		content.setHeader("Content-Type", "text/html; charset=" + MIME_CHARSET);
		content.addHeader("Content-Transfer-Encoding", "base64");
		
		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(content);
		return multipart;
	}
	
	/**
	 * M�todo respons�vel por adicionar o template padr�o ao conteudo do e-mail enviado.
	 * O template pode não ser adicionado caso o atributo "usarTemplate" esteja marcado como false.
	 * 
	 * @param text Conteudo que ser� enviado por e-mail.
	 * 
	 * @return conte�do com o template j� adicionado.
	 * 
	 * @throws UnsupportedEncodingException 
	 */
	private String addTemplate(Map<String, Object> text) throws UnsupportedEncodingException{
		String aSerEnviado = text.toString();
		
		if(template != null){
			logger.info("Adicionando template ao e-mail.");
			aSerEnviado = template.adicionar(text);
		}
		return new String(aSerEnviado.getBytes(), MIME_CHARSET);
	}
	
	/**
	 * M�todo respons�vel por obter a sess�o do java mail.
	 * 
	 * @return sess�o do java mail que ser� usada para enviar o e-mail.
	 */
	private Session getSession(){
		Session session = Session.getDefaultInstance(this.getProperties(), this.getAuthenticator());  
        session.setDebug(true);
        return session;
	}
	
	/**
	 * M�todo respons�vel por obter o autenticador de e-mail.
	 *  
	 * @return autenticador do e-mail.
	 */
	private Authenticator getAuthenticator(){
		return new MailAuthenticator(this.user.getAddress(), this.password);
	}
	
	/**
	 * M�todo respons�vel por obter as propriedades do envio de e-mail.
	 * 
	 * @return propriedades do envio de e-mail.
	 */
	private Properties getProperties(){
    	Properties properties = new Properties();
    	properties.put("mail.transport.protocol", "smtp"); 				//define protocolo de envio como SMTP.  
    	properties.put("mail.smtp.auth", String.valueOf(this.auth)); 					//ativa autenticação.
    	properties.put("mail.smtp.host", this.host); 					//server SMTP do servidor.  
    	properties.put("mail.smtp.port", String.valueOf(this.port)); 					//porta  
    	properties.put("mail.smtp.user", this.user.getAddress()); 		//usuario ou seja, a conta que esta enviando o email.
    	properties.put("mail.smtp.socketFactory.port", String.valueOf(this.port));
    	properties.put("mail.smtp.socketFactory.fallback", String.valueOf(false));
    	
    	if(this.ssl == true){
    		properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
    		properties.put("mail.smtp.ssl.enable", true);
    		properties.put("mail.smtp.starttls.enable", true);
    	}
    	
    	properties.put("mail.mime.charset", MIME_CHARSET);
    	properties.put("mail.debug", DEBUGGING);  
    	properties.put("mail.smtp.debug", DEBUGGING);
    	
    	return properties;
	}
	

	/*
	 * 	Override
	 ***********************/
	@Override
	public boolean equals(Object object){
		if(object instanceof SendMail == false){
			return false;
		}else if(this.user != null){
			return this.user.equals(((SendMail) object).getUser());
		}
		return super.equals(object);
	}

	@Override
	public int hashCode(){
		if(this.user != null){
			return this.user.hashCode();
		}
		return super.hashCode();
	}

	@Override
	public String toString(){
		if(this.user != null){
			return this.user.getAddress();
		}
		return super.toString();
	}
	

	/*	
	 * Getters e Setter
	 ***********************/
	public InternetAddress getUser(){
		return user;
	}
}