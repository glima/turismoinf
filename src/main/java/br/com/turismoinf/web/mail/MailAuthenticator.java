package br.com.turismoinf.web.mail;

import java.io.Serializable;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

import com.google.common.base.Strings;

class MailAuthenticator extends Authenticator implements Serializable{
	
	/*	
	 * Constantes
	 ***********************/
	private static final long serialVersionUID = 345336501633479697L;
	
	/*	
	 * Atributos
	 ***********************/
	private String user;
	private String password;
	
	/*	
	 * Construtores
	 ***********************/
	/**
	 * Construtor padr�o.
	 * 
	 * 
	 * @param user usu�rio do servidor de e-mail.
	 * @param password senha do usu�rio do servidor de e-mail.
	 */
	public MailAuthenticator(String user, String password){
		super();
		this.user = user;
		this.password = password;
	}
	
	/*	
	 * M�todos
	 ***********************/
	/**
	 * M�todo respons�vel por retornar a autentica��o do e-mail.
	 * 
	 */
	@Override
	protected PasswordAuthentication getPasswordAuthentication(){
		return new PasswordAuthentication (user, password);
	}
	
	/*	
	 * Override
	 ***********************/
	@Override
	public boolean equals(Object object){
		if(object instanceof MailAuthenticator == false){
			return false;
		}else if(Strings.isNullOrEmpty(this.user) == false){
			return this.user.equals(((MailAuthenticator) object).getUser());
		}
		return super.equals(object);
	}

	@Override
	public int hashCode(){
		if(Strings.isNullOrEmpty(this.user) == false){
			return this.user.hashCode();
		}
		return super.hashCode();
	}

	@Override
	public String toString(){
		if(Strings.isNullOrEmpty(this.user) == false){
			return this.user;
		}
		return super.toString();
	}

	/*	
	 * Getters e Setters
	 ***********************/
	public String getUser(){
		return user;
	}
}
