package br.com.turismoinf.web.mail;

import java.io.Serializable;

import javax.mail.internet.InternetAddress;

public class ConfigMail implements Serializable {

	/*
	 * Constantes
	 ***********************/
	private static final long serialVersionUID = 451352072159206851L;

	/*
	 * Propriedades
	 ***********************/
	private InternetAddress usuario;
	private String senha;
	private String servidor;
	private int porta;
	private boolean ssl;
	
	/*
	 * Construtores
	 ***********************/
	
	/**
	 * Construtor que recebe todas as propriedades
	 * 
	 * @param usuario
	 * @param senha
	 * @param servidor
	 * @param porta
	 * @param ssl
	 */
	public ConfigMail(InternetAddress usuario, String senha, String servidor, int porta, boolean ssl) {
		this.usuario = usuario;
		this.senha = senha;
		this.servidor = servidor;
		this.porta = porta;
		this.ssl = ssl;
	}

	/*
	 * Getters e Setters
	 ***********************/
	public InternetAddress getUsuario() {
		return usuario;
	}

	public void setUsuario(InternetAddress usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getServidor() {
		return servidor;
	}

	public void setServidor(String servidor) {
		this.servidor = servidor;
	}

	public int getPorta() {
		return porta;
	}

	public void setPorta(int porta) {
		this.porta = porta;
	}

	public boolean isSsl() {
		return ssl;
	}

	public void setSsl(boolean ssl) {
		this.ssl = ssl;
	}
}
