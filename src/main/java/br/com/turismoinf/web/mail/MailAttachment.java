package br.com.turismoinf.web.mail;

import java.io.Serializable;

import javax.activation.DataHandler;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.util.ByteArrayDataSource;

public class MailAttachment implements Serializable{


	/*	
	 * Contantes
	 ***********************/
	private static final long serialVersionUID = -3962254935586057019L;
	

	/*	
	 * Atributos
	 ***********************/	
	private byte[] conteudo;
	private String nome;
	private FileType fileType;
	

	/*	
	 * Construtores
	 ***********************/
	
	/**
	 * Construtor padr�o.
	 * 
	 * @param conteudo array de bytes do arquivo a ser anexado ao e-mail.
	 * @param nome nome do arquivo a ser anexado ao e-mail.
	 * @param fileType tipo do arquivo a ser anexado ao e-mail.
	 */
	public MailAttachment(byte[] conteudo, String nome, FileType fileType){
		super();
		this.conteudo = conteudo;
		this.nome = nome;
		this.fileType = fileType;
	}

	/*	
	 * M�todos
	 ***********************/
	
	/**
	 * M�todo restpons�vel por criar a parte do corpo que contem o arquivo que vai em anexo.
	 * 
	 * @throws MessagingException
	 * 
	 * @return parte do corpo de e-mail que contem o arquivo que vai em anexo.
	 */
	public MimeBodyPart getMimeBodyPart() throws MessagingException{
		MimeBodyPart attach = new MimeBodyPart();
		attach.setDataHandler( new DataHandler(new ByteArrayDataSource(this.conteudo, this.fileType.getMimeType())));
		attach.setFileName(this.nome);
		return attach;
	}

	/*	
	 * Getters e Setter
	 ***********************/
	
	public byte[] getConteudo(){
		return conteudo;
	}
	
	public void setConteudo(byte[] conteudo){
		this.conteudo = conteudo;
	}
	
	public String getNome(){
		return nome;
	}
	
	public void setNome(String nome){
		this.nome = nome;
	}
	
	public FileType getFileType(){
		return fileType;
	}
	
	public void setFileType(FileType fileType){
		this.fileType = fileType;
	}	
}
