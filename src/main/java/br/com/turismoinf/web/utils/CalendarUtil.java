package br.com.turismoinf.web.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Classe utilit�ria para manipula��o de Calendar
 * 
 * @since 1.0.0
 */
public final class CalendarUtil {

	/*
	 * M�todos
	 *********************/

	/**
	 * Obt�m data em um determinado formato
	 * 
	 * @param formato
	 *            - formato desejado
	 * @param calendar
	 *            - calendar a ser formatado
	 * @since 1.0.0
	 * 
	 * @return Retorna data no formato desejado
	 */
	public static final String formata(String formato, Calendar calendar) {
		SimpleDateFormat formatador = new SimpleDateFormat();
		formatador.applyPattern(formato);
		return formatador.format(calendar.getTime()).toString();
	}

	/**
	 * Converte um tipo Date em um tipo Calendar
	 * 
	 * @param data
	 *            - data a ser convertida
	 * @return Retorna um tipo Date em um tipo Calendar
	 */
	public static final Calendar parseCalendar(Date data) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);

		return calendar;
	}

	/**
	 * Obt�m horas em minutos
	 * 
	 * @param calendar
	 *            - calendar a se obter as horas em minutos
	 * @since 1.0.0
	 * 
	 * @return Retorna as horas do calendar em minutos
	 */
	public static final int getHoraMinutos(Calendar calendar) {
		int hora = calendar.get(Calendar.HOUR_OF_DAY);

		return (hora * 60);
	}

	/**
	 * Obt�m horas em segundos
	 * 
	 * @param calendar
	 *            - calendar a se obter as horas em segundos
	 * @since 1.0.0
	 * 
	 * @return Retorna as horas do calendar em segundos
	 */
	public static final int getHoraSegundos(Calendar calendar) {
		int hora = calendar.get(Calendar.HOUR_OF_DAY);

		return ((hora * 60) * 60);
	}

	/**
	 * Obt�m o hor�rio em minutos
	 * 
	 * @param horario
	 *            - hor�rio a ser extra�do o total em minutos
	 * @since 1.0.0
	 * 
	 * @return Retorna o hor�rio em total de minutos
	 */
	public static final int getHorarioEmMinutos(Calendar horario) {
		return (getHoraMinutos(horario) + horario.get(Calendar.MINUTE));
	}

	/**
	 * Obt�m o hor�rio em segundos
	 * 
	 * @param horario
	 *            - hor�rio a ser extra�do o total em segundos
	 * @since 1.0.0
	 * 
	 * @return Retorna o hor�rio em total de segundos
	 */
	public static final int getHorarioEmSegundos(Calendar horario) {
		return (getHoraSegundos(horario) + horario.get(Calendar.SECOND));
	}

	/**
	 * Converte minutos para um calendar
	 * 
	 * @param minutos
	 *            - quantidade em minutos
	 * @since 1.0.0
	 * 
	 * @return Retorna um objeto de Calendar representando a quantidade em
	 *         minutos
	 */
	public static final Calendar minutosParaCalendar(int minutos) {
		Calendar calendar = Calendar.getInstance();

		int hora = minutos / 60;
		int minuto = minutos - (hora * 60);

		calendar.set(Calendar.HOUR_OF_DAY, hora);
		calendar.set(Calendar.MINUTE, minuto);

		return calendar;
	}

	/**
	 * Converte segundos para um calendar
	 * 
	 * @param minutos
	 *            - quantidade em segundos
	 * @since 1.0.0
	 * 
	 * @return Retorna um objeto de Calendar representando a quantidade em
	 *         segundos
	 */
	public static final Calendar segundosParaCalendar(int segundos) {
		Calendar calendar = Calendar.getInstance();

		int hora = segundos / 3600;
		int minuto = segundos - (hora * 3600) / 60;
		int segundo = segundos - (hora * 3600) - (minuto * 60);

		calendar.set(Calendar.HOUR_OF_DAY, hora);
		calendar.set(Calendar.MINUTE, minuto);
		calendar.set(Calendar.SECOND, segundo);

		return calendar;
	}

	/**
	 * Converte milisegundos para um calendar
	 * 
	 * @param minutos
	 *            - quantidade em milisegundos
	 * @since 1.0.0
	 * 
	 * @return Retorna um objeto de Calendar representando a quantidade em
	 *         milisegundos
	 */
	public static final Calendar milisegundosParaCalendar(int milisegundos) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milisegundos);

		return calendar;
	}

	/**
	 * M�todo respons�vel por retornar a diferen�a entre duas datas em minutos.
	 * 
	 * @param calendarioInicial
	 *            - data inicio, ou menor data.
	 * @param calendarioFinal
	 *            - data final, ou maior data.
	 * 
	 * @return diferen�a em minutos.
	 */
	public final static long diferencaMinutos(final Calendar calendarioInicial, final Calendar calendarioFinal) {
		return diferencaMiliSegundos(calendarioInicial, calendarioFinal) / Long.valueOf("60000"); // 1
																									// minuto
																									// =
																									// 60000
																									// milisegundos.
	}

	/**
	 * M�todo respons�vel por retornar a diferen�a entre duas datas em segundos.
	 * 
	 * @param calendarioInicial
	 *            - data inicio, ou menor data.
	 * @param calendarioFinal
	 *            - data final, ou maior data.
	 * 
	 * @return diferen�a em segundos.
	 */
	public final static long diferencaSegundos(final Calendar calendarioInicial, final Calendar calendarioFinal) {
		return diferencaMiliSegundos(calendarioInicial, calendarioFinal) / Long.valueOf("1000"); // 1
																									// segundo
																									// =
																									// 1000
																									// milisegundos.
	}

	/**
	 * M�todo respons�vel por retornar a diferen�a entre duas datas em horas.
	 * 
	 * @param calendarioInicial
	 *            - data inicio, ou menor data.
	 * @param calendarioFinal
	 *            - data final, ou maior data.
	 * 
	 * @return diferen�a em horas.
	 */
	public final static float diferencaHoras(final Calendar calendarioInicial, final Calendar calendarioFinal) {
		return (float) diferencaMiliSegundos(calendarioInicial, calendarioFinal) / Long.valueOf("3600000"); // 1
																											// hora
																											// =
																											// 3600000
																											// milisegundos.
	}

	/**
	 * M�todo respons�vel por retornar a diferen�a entre duas datas em dias
	 * 
	 * @param calendarioInicial
	 *            - data inicio, ou menor data.
	 * @param calendarioFinal
	 *            - data final, ou maior data.
	 * 
	 * @return diferen�a em dias
	 */
	public final static long diferencaDias(Calendar calendarioInicial, Calendar calendarioFinal) {
		return diferencaMiliSegundos(calendarioInicial, calendarioFinal) / Long.valueOf("86400000"); // 1
																										// dia
																										// =
																										// 86400000
																										// milisegubdos
	}

	/**
	 * M�todo respons�vel por retornar a diferen�a entre duas datas em
	 * milisegundos.
	 * 
	 * @param calendarioInicial
	 *            - data inicio, ou menor data.
	 * @param calendarioFinal
	 *            - data final, ou maior data.
	 * 
	 * @return diferen�a em milisegundos.
	 */
	public final static long diferencaMiliSegundos(final Calendar calendarioInicial, final Calendar calendarioFinal) {
		return calendarioFinal.getTimeInMillis() - calendarioInicial.getTimeInMillis();
	}

	/**
	 * Converte uma data em String para Calendar com horas e minutos e segundos
	 * 
	 * @param data
	 *            - data a ser convertida
	 * @return Retorna data como Calendar
	 */
	public final static Calendar parseCalendarDateTime(String data) {
		SimpleDateFormat formatador = new SimpleDateFormat();
		formatador.applyPattern("yyyy-MM-dd HH:mm:ss.SS");
		Calendar cal = Calendar.getInstance();
		try {
			cal.setTime(formatador.parse(data));

			return cal;
		} catch (ParseException e) {
			System.err.println(e.getMessage());
			return null;
		}
	}

	/**
	 * Converte uma data em String para Calendar com horas e minutos
	 * 
	 * @param data
	 *            - formato a ser modelado para o calendar
	 * @param data
	 *            - data a ser convertida
	 * @return Retorna data como Calendar
	 */
	public final static Calendar parseCalendarDateTime(String formato, String data) {
		SimpleDateFormat formatador = new SimpleDateFormat();
		formatador.applyPattern(formato);
		Calendar cal = Calendar.getInstance();
		try {
			cal.setTime(formatador.parse(data));

			return cal;
		} catch (ParseException e) {
			return null;
		}
	}

	/**
	 * Converte uma data em String para Calendar no formato yyyy-MM-dd
	 * 
	 * @param data
	 *            - data a ser convertida
	 * @return Retorna data como Calendar
	 */
	public final static Calendar parseCalendarDate(String data) {
		SimpleDateFormat formatador = new SimpleDateFormat();
		formatador.applyPattern("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();

		try {

			cal.setTime(formatador.parse(data));

			return cal;
		} catch (ParseException e) {
			return null;
		}
	}

	/**
	 * Converte uma data em String para Calendar no formato dd/MM/yyyy
	 * 
	 * @param data
	 *            - data a ser convertida
	 * @return Retorna data como Calendar
	 */
	public final static Calendar parseCalendarDateddMMyyyy(String data) {
		SimpleDateFormat formatador = new SimpleDateFormat();
		formatador.applyPattern("dd/MM/yyyy");
		Calendar cal = Calendar.getInstance();
		try {
			cal.setTime(formatador.parse(data));

			return cal;
		} catch (ParseException e) {
			return null;
		}
	}

	/**
	 * Verifica se duas datas s�o do mesmo dia
	 * 
	 * @param calendar1
	 *            - data 1
	 * @param calendar2
	 *            - data 2
	 * 
	 * @return Retorna true caso sejam do mesmo dia, ou false caso contr�rio
	 */
	public final static boolean ehMesmoDia(Calendar calendar1, Calendar calendar2) {
		int dia = calendar1.get(Calendar.DAY_OF_MONTH);
		int mes = calendar1.get(Calendar.MONTH);
		int ano = calendar1.get(Calendar.YEAR);

		if (calendar2.get(Calendar.DAY_OF_MONTH) == dia && calendar2.get(Calendar.MONTH) == mes
				&& calendar2.get(Calendar.YEAR) == ano) {
			return true;
		}

		return false;
	}

	/**
	 * Obt�m lista de datas de um intervalo de datas no formato Br
	 * 
	 * @param dataInicial
	 *            - data do in�cio do intervalo
	 * @param dataFinal
	 *            - data do fim do intervalo
	 * 
	 * @return Retorna lista de datas deste intervalo
	 */
	public final static List<String> getDatasIntervaloBr(Calendar dataInicial, Calendar dataFinal) {
		Calendar anterior = (Calendar) dataInicial.clone();

		Set<String> lista = new LinkedHashSet<String>();

		while (anterior.before(dataFinal)) {
			lista.add(formata("dd/MM/yyyy", anterior));

			anterior.add(Calendar.DAY_OF_MONTH, 1);
		}

		lista.add(formata("dd/MM/yyyy", dataFinal));

		return new LinkedList<String>(lista);
	}

	/**
	 * Define data para o �nicio do dia
	 * 
	 * @param data
	 *            - data a ser definida
	 * @return Retorna data no in�cio do dia
	 */
	public final static Calendar defineInicioDia(Calendar data) {
		Calendar inicioDia = (Calendar) data.clone();

		inicioDia.set(Calendar.HOUR_OF_DAY, 0);
		inicioDia.set(Calendar.MINUTE, 0);
		inicioDia.set(Calendar.SECOND, 0);
		inicioDia.set(Calendar.MILLISECOND, 0);

		return inicioDia;
	}

	/**
	 * Define data para o fim do dia
	 * 
	 * @param data
	 *            - data a ser definida
	 * @return Retorna data no fim do dia
	 */
	public final static Calendar defineFimDia(Calendar data) {
		Calendar fimDia = (Calendar) data.clone();

		fimDia.set(Calendar.HOUR_OF_DAY, 23);
		fimDia.set(Calendar.MINUTE, 59);
		fimDia.set(Calendar.SECOND, 59);
		fimDia.set(Calendar.MILLISECOND, 999);

		return fimDia;
	}

	/**
	 * Formata a data para escrita no excel din�mico
	 * 
	 * @param data
	 *            - data no formato yyyy-MM-dd
	 * @return Retorna String com o formato dd/MM/yyyy
	 */
	public final static String formatarStringData(String data) {
		//2015-11-25
		//25/11/2015
		String[] dataFormatada = data.split("-");
		if (dataFormatada.length == 3) {
			return dataFormatada[2] + "/" + dataFormatada[1] + "/" + dataFormatada[0];
		}

		return null;
	}
}
