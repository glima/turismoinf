package br.com.turismoinf.web.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.codec.binary.Base64;

public final class FileUtil {

	public static String getBase64(InputStream finput, File file) throws IOException {
		byte[] imageBytes = new byte[(int)file.length()];
		finput.read(imageBytes, 0, imageBytes.length);
		finput.close();
		String imageStr = Base64.encodeBase64String(imageBytes);
		return imageStr;
	}
}
