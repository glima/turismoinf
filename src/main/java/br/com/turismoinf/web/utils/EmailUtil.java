package br.com.turismoinf.web.utils;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import br.com.turismoinf.web.mail.ConfigMail;
import br.com.turismoinf.web.mail.SendMail;
import br.com.turismoinf.web.mail.TemplateMail;

public final class EmailUtil {

	public static final ConfigMail getConfigEmail() throws AddressException {

		InternetAddress userEmail = new InternetAddress("gladston.sist@gmail.com");
		String passwd = "?/S1st3m4s/13c0unt27/";
		Integer port = 465;

		String server = "smtp.gmail.com";

		ConfigMail configMail = new ConfigMail(userEmail, passwd, server, port, true);

		return configMail;
	}

	public static boolean isValidateEmail(String email) {  
	    Pattern pattern = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,7}$");   
	    Matcher matcher = pattern.matcher(email);   
	    return matcher.find();   
	} 
	
	public static boolean isValidateServer(String server) {  
	    Pattern pattern = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*\\.([\\w-]+\\.)+[a-zA-Z]{2,7}$");   
	    Matcher matcher = pattern.matcher(server);   
	    return matcher.find();   
	} 
	
	public static void envia(ConfigMail configMail, String assunto, Map<String, Object> corpo, List<String> destinatarios, TemplateMail templateMail) 
			throws UnsupportedEncodingException, MessagingException {
		
		SendMail mail = new SendMail(configMail);
		List<Address> enderecos = new ArrayList<Address>();
		
		for (String address : destinatarios) {
			enderecos.add(new InternetAddress(address));
		}
		
		mail.send(assunto, corpo, enderecos, templateMail);
	}
}
