package br.com.turismoinf.web.domain.models.place;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@javax.persistence.Entity
@Table(name = "fotos_local")
public class PlacePhoto implements Serializable {

	private static final long serialVersionUID = 4862082143220001172L;
	
	@Id
	@Basic(optional = false)
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Basic(optional = false)
	@JoinColumn(name = "local_id", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private Place place;
	
	@Basic(optional = false)
	@Column(name = "imagem")
	private String image;
	
	@Basic(optional = false)
	@Column(name = "dt_criacao")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar dateCreation;
	
	public PlacePhoto() {
		this.dateCreation = Calendar.getInstance();
	}
	
	public PlacePhoto(String address, Place place) {
		this();
		this.image = address;
		this.place = place;
	}
	
	@Override
	public int hashCode() {
		return image.hashCode();
	}
	
	@Override
	public boolean equals(Object object) {
		if (!(object instanceof PlacePhoto)) {
            return false;
        }
		PlacePhoto other = (PlacePhoto) object;
        if ((this.image == null && other.image != null) || (this.image != null && !this.image.equals(other.image))) {
            return false;
        }
        
        if(this.place.getId().equals(other.getPlace().getId())) {
        	
        }
        
        return true;
	}

	public Place getPlace() {
		return place;
	}

	public void setPlace(Place place) {
		this.place = place;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String address) {
		this.image = address;
	}

	public Calendar getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Calendar dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	
}
