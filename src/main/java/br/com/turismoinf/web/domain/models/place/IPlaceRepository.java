package br.com.turismoinf.web.domain.models.place;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IPlaceRepository extends PagingAndSortingRepository<Place, Long> {
	
	@Query("select o from Place o")
	public List<Place> findAll();
	
	@Query("select o from Place o join o.categories c where c.id = :category")
	public List<Place> findAll(@Param("category") long idCategory);
	
	@Query("select o from Place o join o.categories c where c.id = :category")
	public List<Place> findAll(@Param("category") long idCategory, Pageable pageable);
	
	@Query("select o from Place o where o.id = :id")
	public Place findById(@Param("id") long id);
	
	@Query("select o from Place o where o.name = :name")
	public Place findByName(@Param("name") String name);
	
	@Modifying
	@Query(value = "delete from fotos_local where local_id = :place", nativeQuery = true)
	public void deletePhotos(@Param("place") long place);
}
