package br.com.turismoinf.web.domain.models.conf;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import br.com.turismoinf.web.utils.EmailUtil;

public class EmailConf {

	private String email;
	private String password;
	private Integer port;
	private String server;
	
	private List<String> errors;
	
	public EmailConf(String email, String password, String server, Integer port) {
		this();
		this.email = email;
		this.password = password;
		this.server = server;
		this.port = port;
	}
	
	public EmailConf() {
		this.errors = new ArrayList<>();
	}
	
	public boolean isValid() {
		if(StringUtils.isBlank(email)
			&& StringUtils.isBlank(password)
			&& StringUtils.isBlank(server)
			&& (port == null || port == 0)) {
			return true;
		}
		
		if(StringUtils.isBlank(email)
			|| StringUtils.isBlank(password)
			|| StringUtils.isBlank(server)
			|| port == null || port == 0) {
			
			this.errors.add("Todos os dados são necessários para a configuração de conta de email.");
		} else {
			if(!EmailUtil.isValidateEmail(email)) {
				this.errors.add("Conta de email inválida.");
			}
			
			if(!EmailUtil.isValidateServer(server)) {
				this.errors.add("Servidor de email inválido.");
			}
			
			if(port > 99999999) {
				this.errors.add("Porta com valor muito longo.");
			}
		}
		
		if(!this.errors.isEmpty()) {
			return false;
		}
		
		return true;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}
}
