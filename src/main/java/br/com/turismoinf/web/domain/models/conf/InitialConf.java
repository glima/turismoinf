package br.com.turismoinf.web.domain.models.conf;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class InitialConf {

	private String photo;
	private String description;
	
	private List<String> errors;
	
	public InitialConf() {
		this.errors = new ArrayList<>();
	}
	
	public InitialConf(String photo, String description) {
		this();
		
		this.photo = photo;
		this.description = description;
	}
	
	public boolean isValid() {
		if(StringUtils.isBlank(description)) {
			this.errors.add("Descrição da cidade deve ser informada.");
		}
		
		return this.errors.isEmpty();
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
	
	
}
