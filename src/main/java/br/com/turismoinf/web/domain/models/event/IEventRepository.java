package br.com.turismoinf.web.domain.models.event;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IEventRepository extends PagingAndSortingRepository<Event, Long> {
	
	@Query("select o from Event o")
	public List<Event> findAll();
	
	@Query("select o from Event o where o.id = :id")
	public Event findById(@Param("id") long id);
	
	@Query("select o from Event o where o.name = :name")
	public Event findByName(@Param("name") String name);
	
	@Modifying
	@Query(value = "delete from fotos_evento where evento_id = :event", nativeQuery = true)
	public void deletePhotos(@Param("event") long event);
}
