package br.com.turismoinf.web.domain.models.user;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IUserRepository extends JpaRepository<User, Long> {

	@Query("select o from br.com.turismoinf.web.domain.models.user.User o")
	public List<User> findAll();
	
	@Query("select o from br.com.turismoinf.web.domain.models.user.User o where o.id = :id")
	public User findById(@Param("id") long id);
	
	@Query("select o from br.com.turismoinf.web.domain.models.user.User o where o.email = :email")
	public User findByEmail(@Param("email") String email);
	
	@Query("select o from br.com.turismoinf.web.domain.models.user.User o where o.login = :username")
	public User findByUsername(@Param("username") String login);
	
}
