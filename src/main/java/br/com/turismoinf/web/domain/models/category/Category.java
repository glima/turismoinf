package br.com.turismoinf.web.domain.models.category;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.turismoinf.web.domain.models.place.Place;

@javax.persistence.Entity
@Table(name = "categoria")
public class Category implements Serializable {

	private static final long serialVersionUID = -8039239787404150252L;

	@Id
	@Basic(optional = false)
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Basic(optional = false)
	@Column(name = "descricao")
	private String description;
	
	@Column(name = "icone")
	private String icon;
	
	@Basic(optional = false)
	@Column(name = "dt_criacao")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar dateCreation;
	
	@ManyToMany(mappedBy = "categories", fetch = FetchType.LAZY)
	private Set<Place> places;
	
	public Category() {
		this.dateCreation = Calendar.getInstance();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Calendar getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Calendar dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Set<Place> getPlaces() {
		return places;
	}

	public void setPlaces(Set<Place> places) {
		this.places = places;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	
}
