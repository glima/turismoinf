package br.com.turismoinf.web.domain.models.user;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IUserClientRepository extends JpaRepository<UserClient, Long> {

	@Query("select o from UserClient o")
	public List<UserClient> findAll();
	
	@Query("select o from UserClient o where o.id = :id")
	public UserClient findById(@Param("id") long id);
	
	@Query("select o from UserClient o where o.email = :email")
	public UserClient findByEmail(@Param("email") String email);
	
}
