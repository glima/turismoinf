package br.com.turismoinf.web.domain.models.place;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.turismoinf.web.domain.models.category.Category;

@javax.persistence.Entity
@Table(name = "local")
public class Place implements Serializable {

	private static final long serialVersionUID = 8417205437990340372L;
	
	@Id
	@Basic(optional = false)
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Basic(optional = false)
	@Column(name = "nome")
	private String name;
	
	@Basic(optional = false)
	@Column(name = "descricao")
	private String description;
	
	@Basic(optional = false)
	@Column(name = "cep")
	private int cep;
	
	@Basic(optional = false)
	@Column(name = "cidade")
	private String city;
	
	@Basic(optional = false)
	@Column(name = "rua")
	private String street;
	
	@Basic(optional = false)
	@Column(name = "numero")
	private int number;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "telefone")
	private Long phone;
	
	@Basic(optional = false)
	@Column(name = "dt_criacao")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar dateCriation;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "local_categoria", joinColumns = { @JoinColumn(name = "local_id") }, inverseJoinColumns = { @JoinColumn(name = "categoria_id") })
	private Set<Category> categories;
	
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "place", cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private Set<PlacePhoto> photos;

	public Place() {
		this.dateCriation = Calendar.getInstance();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getCep() {
		return cep;
	}

	public void setCep(int cep) {
		this.cep = cep;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public Calendar getDateCriation() {
		return dateCriation;
	}

	public void setDateCriation(Calendar dateCriation) {
		this.dateCriation = dateCriation;
	}

	public Set<Category> getCategories() {
		return categories;
	}

	public void setCategories(Set<Category> categories) {
		this.categories = categories;
	}

	public Set<PlacePhoto> getPhotos() {
		return photos;
	}

	public void setPhotos(Set<PlacePhoto> photos) {
		this.photos = photos;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getPhone() {
		return phone;
	}

	public void setPhone(Long phone) {
		this.phone = phone;
	}
	
}
