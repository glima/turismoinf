package br.com.turismoinf.web.domain.models.event;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@javax.persistence.Entity
@Table(name = "fotos_evento")
public class EventPhoto implements Serializable {

	private static final long serialVersionUID = -7104731612909860559L;

	@Id
	@Basic(optional = false)
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Basic(optional = false)
	@JoinColumn(name = "evento_id", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private Event event;
	
	@Basic(optional = false)
	@Column(name = "imagem")
	private String image;
	
	@Basic(optional = false)
	@Column(name = "dt_criacao")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar dateCreation;
	
	public EventPhoto() {
		this.dateCreation = Calendar.getInstance();
	}
	
	public EventPhoto(String address, Event event) {
		this();
		this.image = address;
		this.event = event;
	}
	
	@Override
	public int hashCode() {
		return image.hashCode();
	}
	
	@Override
	public boolean equals(Object object) {
		if (!(object instanceof EventPhoto)) {
            return false;
        }
		EventPhoto other = (EventPhoto) object;
        if ((this.image == null && other.image != null) || (this.image != null && !this.image.equals(other.image))) {
            return false;
        }
        
        if(this.event.getId().equals(other.getEvent().getId())) {
        	
        }
        
        return true;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String address) {
		this.image = address;
	}

	public Calendar getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Calendar dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	
}
