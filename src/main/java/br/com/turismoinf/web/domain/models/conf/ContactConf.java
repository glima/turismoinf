package br.com.turismoinf.web.domain.models.conf;

import java.util.ArrayList;
import java.util.List;

import br.com.turismoinf.web.utils.EmailUtil;

public class ContactConf {

	private String email;
	private Long phone;
	
	private List<String> errors;
	
	public ContactConf() {
		this.setErrors(new ArrayList<>());
	}
	
	public ContactConf(String email, Long phone) {
		this();
		this.email = email;
		this.phone = phone;
	}
	
	public boolean isValid() {
		if(email != null && !EmailUtil.isValidateEmail(email)) {
			this.errors.add("Email inválido.");
		}
		
		if(phone != null && phone.toString().length() != 11) {
			errors.add("Telefone inválido. Informe um telefone junto ao DDD. Ex: 3199887766.");
		}
		
		if(!this.errors.isEmpty()) {
			return false;
		}
		
		return true;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getPhone() {
		return phone;
	}

	public void setPhone(Long phone) {
		this.phone = phone;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
	
	
}
