package br.com.turismoinf.web.domain.models.category;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ICategoryRepository extends JpaRepository<Category, Long> {

	@Query("select o from Category o where o.description = :description")
	public Category findByDescription(@Param("description") String description);
	
	@Query("select o from Category o where o.id in (:ids)")
	public List<Category> findByIds(@Param("ids") List<Long> ids);
}
