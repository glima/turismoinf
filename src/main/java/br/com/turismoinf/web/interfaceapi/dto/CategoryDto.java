package br.com.turismoinf.web.interfaceapi.dto;

import org.json.JSONObject;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class CategoryDto {

	private long id;
	private String description;
	private String image;
	private boolean hasImage;
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public static CategoryDto parse(JSONObject json) {
		CategoryDto dto = new CategoryDto();
		
		dto.setDescription(json.has("description") ? json.getString("description") : null);
		
		return dto;
	}

	public boolean isHasImage() {
		return hasImage;
	}

	public void setHasImage(boolean hasImage) {
		this.hasImage = hasImage;
	}
}