package br.com.turismoinf.web.interfaceapi.facade;

import java.util.List;

import br.com.turismoinf.web.interfaceapi.dto.PlaceDto;

public interface IPlaceFacade {

	public Result<PlaceDto> addPlace(PlaceDto categoryDto);
	
	public List<String> deletePlace(long id);
	
	public List<String> updatePlace(long id, PlaceDto categoryDto);
	
	public PlaceDto findPlace(long id);
	
	public List<String> getPhotos(long id);
	
	public List<PlaceDto> listPlaces();
	
	public String getPlacePhoto(long id);
}
