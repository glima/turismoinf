package br.com.turismoinf.web.interfaceapi.facade.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;
import javax.inject.Inject;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import br.com.turismoinf.web.application.service.ICategoryService;
import br.com.turismoinf.web.domain.models.category.Category;
import br.com.turismoinf.web.domain.models.category.ICategoryRepository;
import br.com.turismoinf.web.infra.dao.impl.ClientDao;
import br.com.turismoinf.web.interfaceapi.dto.CategoryDto;
import br.com.turismoinf.web.interfaceapi.dto.PlaceDto;
import br.com.turismoinf.web.interfaceapi.dto.parse.CategoryDtoParse;
import br.com.turismoinf.web.interfaceapi.facade.ICategoryFacade;
import br.com.turismoinf.web.interfaceapi.facade.Result;

@Service
public class CategoryFacade implements ICategoryFacade {
	
	@Inject private ICategoryRepository categoryRepository;
	@Inject private ICategoryService categoryService;
	
	@Inject @Resource(name = "clientDao") private ClientDao clientDao;
	
	@Override
	public List<PlaceDto> listPlaces(long id, int page) {
		List<PlaceDto> outputs = this.clientDao.listPlaces(id, page);
		
		for (PlaceDto place : outputs) {
			if(!place.getImages().isEmpty()) {
				place.setImages(Arrays.asList(place.getImages().get(0)));
			}
		}
		
		return outputs;
	}
	
	@Override
	public List<CategoryDto> listCategories(int page) {
		List<CategoryDto> listCategories = this.clientDao.listCategories(page);
		
		return listCategories;
	}
	
	@Override
	public List<CategoryDto> listCategories() {
		List<Category> outputs = this.categoryRepository.findAll();
		
		CategoryDtoParse dtoParse = new CategoryDtoParse();
		
		List<CategoryDto> dtos = new ArrayList<>();
		for (Category category : outputs) {
			dtos.add(dtoParse.parseModelToDtoMinified(category));
		}
		
		return dtos;
	}

	@Override
	public Result<CategoryDto> addCategory(CategoryDto categoryDto) {

		List<String> errors = this.categoryService.canCreate(categoryDto.getDescription());
		
		if(errors.isEmpty()) {
			Category category = this.categoryService.createCategory(categoryDto.getDescription(), categoryDto.getImage());
			return new Result<CategoryDto>(new CategoryDtoParse().parseModelToDto(category));
		}
		
		return new Result<CategoryDto>(errors);
	}

	@Override
	public List<String> deleteCategory(long id) {
		List<String> errors = this.categoryService.canDelete(id);
		
		if(errors.isEmpty()) {
			try {
				this.categoryService.deleteCategory(id);
			} catch (DataIntegrityViolationException e) {
				errors.add("Categoria não pode ser excluída pois possui dados associados.");
			}
		}
		
		return errors;
	}

	@Override
	public List<String> updateCategory(long id, CategoryDto categoryDto) {
		List<String> errors = this.categoryService.canUpdate(id, categoryDto.getDescription());
		
		if(errors.isEmpty()) {
			this.categoryService.updateCategory(id, categoryDto.getDescription(), categoryDto.getImage());
		}
		
		return errors;
	}

	@Override
	public CategoryDto findCategory(long id) {
		Category category = this.categoryRepository.findOne(id);

		CategoryDto dto = null;
		if(category != null) {
			dto = new CategoryDtoParse().parseModelToDto(category);
		}
		
		return dto;
	}

	@Override
	public String getCategoryPhoto(long id) {
		String image = this.clientDao.getCategoryImage(id);
		
		return image;
	}

}
