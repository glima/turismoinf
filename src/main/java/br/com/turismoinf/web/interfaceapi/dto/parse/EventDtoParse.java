package br.com.turismoinf.web.interfaceapi.dto.parse;

import java.util.ArrayList;
import java.util.List;

import br.com.turismoinf.web.domain.models.event.Event;
import br.com.turismoinf.web.interfaceapi.dto.ListDto;
import br.com.turismoinf.web.utils.CalendarUtil;
import br.com.turismoinf.web.interfaceapi.dto.EventDto;

public class EventDtoParse {

	public EventDto parseModelToDto(Event event) {
		EventDto dto = new EventDto();
		
		dto.setId(event.getId());
		dto.setDescription(event.getDescription());
		dto.setCep(event.getCep());
		dto.setCity(event.getCity());
		dto.setNumber(event.getNumber());
		dto.setStreet(event.getStreet());
		dto.setName(event.getName());
		dto.setEmail(event.getEmail());
		dto.setPhone(event.getPhone());
		
		List<ListDto> categories = new ArrayList<>();
//		event.getCategories().forEach(item -> {
//			categories.add(new ListDto(item.getId(), item.getDescription()));
//		});
		
		dto.setCategories(categories);
		
		List<String> photos = new ArrayList<>();
		event.getPhotos().forEach(item -> {
			photos.add(item.getImage());
		});
		
		dto.setImages(photos);
		dto.setDateStart(CalendarUtil.formata("yyyy-MM-dd", event.getDateStart()));
		
		if(event.getDateEnd() != null) {
			dto.setDateEnd(CalendarUtil.formata("yyyy-MM-dd", event.getDateEnd()));
		}
		
		dto.setTimeStart(CalendarUtil.formata("HH:mm", CalendarUtil.minutosParaCalendar(event.getTimeStart())));
		
		if(event.getTimeEnd() != null) {
			dto.setTimeEnd(CalendarUtil.formata("HH:mm", CalendarUtil.minutosParaCalendar(event.getTimeEnd())));	
		}
		
		dto.setPrice(event.getPrice());
		
		return dto;
	}
	
	public EventDto parseModelToDtoMinified(Event event) {
		EventDto dto = new EventDto();
		
		dto.setId(event.getId());
		dto.setName(event.getName());		
		
		return dto;
	}
	
	public EventDto parseModelToDtoMinifiedPhoto(Event event) {
		EventDto dto = new EventDto();
		
		dto.setId(event.getId());
		dto.setDescription(event.getDescription());
		dto.setCep(event.getCep());
		dto.setCity(event.getCity());
		dto.setNumber(event.getNumber());
		dto.setStreet(event.getStreet());
		dto.setName(event.getName());
		
		List<ListDto> categories = new ArrayList<>();
//		event.getCategories().forEach(item -> {
//			categories.add(new ListDto(item.getId(), item.getDescription()));
//		});
		
		dto.setCategories(categories);
		
		List<String> photos = new ArrayList<>();
		if(!event.getPhotos().isEmpty()) {
			photos.add(new ArrayList<>(event.getPhotos()).get(0).getImage());
		}
		
		dto.setImages(photos);
		
		return dto;
	}
	
}
