package br.com.turismoinf.web.interfaceapi.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.media.multipart.BodyPartEntity;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.json.JSONObject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import br.com.turismoinf.web.interfaceapi.dto.ConfigurationDto;
import br.com.turismoinf.web.interfaceapi.facade.IConfigurationFacade;
import br.com.turismoinf.web.interfaceapi.facade.Result;

@Component
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
@Path("/adm/configurations")
public class ConfigurationController {
	
	@Inject private IConfigurationFacade confFacade;
	
	@Path("/")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ConfigurationDto getConf() {
		return this.confFacade.getConfiguration();
	}

	@Path("/")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response updateConf(final FormDataMultiPart multiPart) throws UnsupportedEncodingException, IOException {
		JSONObject jsonPlace = new JSONObject(multiPart.getField("conf").getValue());
		FormDataBodyPart bodyParts = multiPart.getField("photo");
		
		boolean wrongType = false;
		String image = null;
		
		if(bodyParts != null) {
			BodyPartEntity bodyPartEntity = (BodyPartEntity) bodyParts.getEntity();
			String type = bodyParts.getMediaType().toString();
			
			if(!type.contains("image")) {
				wrongType = true;
			}
						
			String base64 = new String(Base64.encodeBase64(IOUtils.toByteArray(bodyPartEntity.getInputStream())), "UTF-8");
			image = ("data:" + type + ";base64," + base64);
		}
		
		if(jsonPlace == null || wrongType) {
			return Response.status(Status.BAD_REQUEST).entity("Dados informados inválidos.").build();
		}
		
		ConfigurationDto confDto = ConfigurationDto.parse(jsonPlace);
		confDto.setImage(image);
		Result<ConfigurationDto> result = this.confFacade.updateConfiguration(confDto);
		
		if(!result.getErrors().isEmpty()) {
			return Response.status(Status.BAD_REQUEST).entity(result.getErrors()).build();
		}
		
		return Response.ok(result.getObject()).build();
	}
}
