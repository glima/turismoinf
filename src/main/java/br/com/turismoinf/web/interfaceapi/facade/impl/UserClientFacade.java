package br.com.turismoinf.web.interfaceapi.facade.impl;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.mail.MessagingException;

import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.turismoinf.web.application.service.IUserClientService;
import br.com.turismoinf.web.domain.models.user.IUserClientRepository;
import br.com.turismoinf.web.domain.models.user.UserClient;
import br.com.turismoinf.web.infra.dao.impl.ConfigurationDao;
import br.com.turismoinf.web.interfaceapi.dto.UserClientDto;
import br.com.turismoinf.web.interfaceapi.dto.parse.UserClientDtoParse;
import br.com.turismoinf.web.interfaceapi.facade.IUserClientFacade;
import br.com.turismoinf.web.interfaceapi.facade.Result;
import br.com.turismoinf.web.mail.ResetPasswordMail;
import br.com.turismoinf.web.security.model.UserContext;
import br.com.turismoinf.web.utils.EmailUtil;

@Service
public class UserClientFacade implements IUserClientFacade {

	@Inject IUserClientRepository userRepository;
	@Inject IUserClientService userService;
	@Inject @Resource(name="confDao") private ConfigurationDao confDao;
	
	public Result<UserClientDto> addUserClient(UserClientDto userDto) {
		List<String> errors = this.userService.canCreate(userDto.getName(), userDto.getPhone(), userDto.getPassword(), 
				userDto.getEmail());
		
		if(!errors.isEmpty()) {
			return new Result<UserClientDto>(errors);
		}
		
		UserClient user = this.userService.createUser(userDto.getName(), userDto.getPhone(), userDto.getPassword(), 
				userDto.getEmail());
		
		return new Result<UserClientDto>(new UserClientDtoParse().parseModelToDto(user));
	}

	@Override
	public List<String> forgottenPassword(String email) {
		List<String> errors = new ArrayList<>();
		
		boolean thereIsEmailConf = this.confDao.thereIsEmailConf();
		if(thereIsEmailConf) {
			UserClient user = this.userRepository.findByEmail(email);
			if(user == null) {
				errors.add("Email não cadastrado.");
			} else {
				Md5PasswordEncoder encoder = new Md5PasswordEncoder();
				String newpass = encoder.encodePassword(email + "turismo", null);
				
				user.setPassword(newpass);
				
				List<String> address = new ArrayList<String>();
				address.add(email);
				
				try {
					Map<String, Object> content = new HashMap<String, Object>();
					content.put(ResetPasswordMail.USERNAME, user.getName());
					content.put(ResetPasswordMail.NEW_PASS, newpass);

					EmailUtil.envia(EmailUtil.getConfigEmail(), "Redefinição de senha", content, address, new ResetPasswordMail());
					
					this.userRepository.save(user);
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				} catch (MessagingException e) {
					e.printStackTrace();
				}
			}
		} else {
			errors.add("Não foi possível enviar um email para redefinição.");
		}
		
		
		
		return errors;
	}

	@Override
	public List<String> resetUserPassword(UserContext userContext, UserClientDto userClientDto) {
		UserClientDto user = (UserClientDto) getUser(userContext);
		List<String> errors = this.userService.canResetPassword(user.getId(), userClientDto.getPassword(), userClientDto.getNewPassword());
		
		if(errors.isEmpty()) {
			this.userService.resetPassword(user.getId(), userClientDto.getPassword(), userClientDto.getNewPassword());
		}
		
		return errors;
	}	
	
	public UserClientDto getUser(UserContext userContext) {
		UserClient user = this.userRepository.findByEmail(userContext.getUsername());
		
		UserClientDto dto = null;
		if(user != null) {
			dto = new UserClientDtoParse().parseModelToDto(user);
		}
		
		return dto;
	}
}
