package br.com.turismoinf.web.interfaceapi.facade.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.com.turismoinf.web.infra.filter.SearchOperation;

public class Facade {


	public String captureValueVerified(final Matcher matcher) {
		return matcher.group(4).replaceAll("_space_", " ");
	}

	public Matcher getMatcher(String search) {
		String operationSetExper = String.join("|", SearchOperation.SIMPLE_OPERATION_SET);
	    Pattern pattern = Pattern.compile("(\\w+?)(" + operationSetExper + ")(\\p{Punct}?)(\\w+?)(\\p{Punct}?),");
	    
	    Matcher matcher = pattern.matcher(search.replaceAll(" ", "_space_") + ",");
        
        return matcher;
	}
}
