package br.com.turismoinf.web.interfaceapi.facade;

import java.util.List;

import br.com.turismoinf.web.interfaceapi.dto.EventDto;

public interface IEventFacade {

	public Result<EventDto> addEvent(EventDto categoryDto);
	
	public List<String> deleteEvent(long id);
	
	public List<String> updateEvent(long id, EventDto categoryDto);
	
	public EventDto findEvent(long id);
	
	public List<String> getPhotos(long id);
	
	public List<EventDto> listEvents();
	
	public String getEventPhoto(long id);

	public List<EventDto> listEvents(int page);
}
