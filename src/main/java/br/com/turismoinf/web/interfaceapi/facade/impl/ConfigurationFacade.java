package br.com.turismoinf.web.interfaceapi.facade.impl;

import java.util.List;

import javax.annotation.Resource;
import javax.inject.Inject;

import org.springframework.stereotype.Service;

import br.com.turismoinf.web.application.service.impl.ConfigurationService;
import br.com.turismoinf.web.domain.models.conf.ContactConf;
import br.com.turismoinf.web.domain.models.conf.EmailConf;
import br.com.turismoinf.web.domain.models.conf.InitialConf;
import br.com.turismoinf.web.infra.dao.impl.ConfigurationDao;
import br.com.turismoinf.web.interfaceapi.dto.ConfigurationDto;
import br.com.turismoinf.web.interfaceapi.facade.IConfigurationFacade;
import br.com.turismoinf.web.interfaceapi.facade.Result;

@Service
public class ConfigurationFacade implements IConfigurationFacade {

	@Inject private ConfigurationService confService;
	@Inject @Resource(name="confDao") private ConfigurationDao confDao;
	
	@Override
	public Result<ConfigurationDto> updateConfiguration(ConfigurationDto dto) {
		EmailConf emailConf = null;
		if(dto.hasEmailConfValue()) {
			emailConf = new EmailConf(dto.getEmail(), dto.getPassword(), dto.getServer(), dto.getPort());
		}
		
		ContactConf contactConf = null;
		if(dto.hasContactConfValue()) {
			contactConf = new ContactConf(dto.getEmailContact(), dto.getPhoneContact());
		}
		
		InitialConf initialConf = null;
		if(dto.hasInitialConfValue()) {
			initialConf = new InitialConf(dto.getImage(), dto.getDescription());
		}
		
		List<String> errors = this.confService.canUpdateConfiguration(emailConf, contactConf, initialConf);
		if(!errors.isEmpty()) {
			return new Result<ConfigurationDto>(errors);
		}
		
		this.confService.updateConfiguration(emailConf, contactConf, initialConf);
		return new Result<ConfigurationDto>(dto);
	}

	@Override
	public ConfigurationDto getConfiguration() {
		return this.confDao.getConfiguration();
	}

	@Override
	public ConfigurationDto getInitialConfiguration() {
		ConfigurationDto dto = this.confDao.getConfiguration();
		
		ConfigurationDto conf = new ConfigurationDto();
		conf.setImage(dto.getImage());
		conf.setDescription(dto.getDescription());
		
		return conf;
	}

	@Override
	public ConfigurationDto getContactConfiguration() {
		ConfigurationDto dto = this.confDao.getConfiguration();
		
		ConfigurationDto conf = new ConfigurationDto();
		conf.setEmailContact(dto.getEmailContact());
		conf.setPhoneContact(dto.getPhoneContact());
		
		return conf;
	}
}
