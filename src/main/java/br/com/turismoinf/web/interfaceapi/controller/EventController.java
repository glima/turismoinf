package br.com.turismoinf.web.interfaceapi.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.media.multipart.BodyPartEntity;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import br.com.turismoinf.web.interfaceapi.dto.EventDto;
import br.com.turismoinf.web.interfaceapi.facade.IEventFacade;
import br.com.turismoinf.web.interfaceapi.facade.Result;

@Component
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
@Path("/adm/events")
public class EventController {

	@Inject private IEventFacade eventFacade;
	
	@Path("/")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response addEvent(final FormDataMultiPart multiPart) throws IOException {
		JSONObject jsonEvent = new JSONObject(multiPart.getField("event").getValue());
		List<FormDataBodyPart> bodyParts = multiPart.getFields("photos");
		
		boolean wrongType = false;
		List<String> images = new ArrayList<>(); 
		for (int i = 0; i < bodyParts.size(); i++) {
			BodyPartEntity bodyPartEntity = (BodyPartEntity) bodyParts.get(i).getEntity();
			String type = bodyParts.get(i).getMediaType().toString();
			
			if(!type.contains("image")) {
				wrongType = true;
			}
						
			String base64 = new String(Base64.encodeBase64(IOUtils.toByteArray(bodyPartEntity.getInputStream())), "UTF-8");
			images.add("data:" + type + ";base64," + base64);
		}
		
		if(jsonEvent == null || wrongType) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		
		EventDto eventDto = EventDto.parse(jsonEvent);
		eventDto.setImages(images);
		Result<EventDto> result = this.eventFacade.addEvent(eventDto);
		
		if(!result.getErrors().isEmpty()) {
			return Response.status(Status.BAD_REQUEST).entity(result.getErrors()).build();
		}
		
		return Response.ok(result.getObject()).build();
	}

	@Path("/{id}")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response updateEvent(@PathParam("id") long id, final FormDataMultiPart multiPart) throws IOException {
		EventDto event = this.eventFacade.findEvent(id);
		
		if(event == null) {
			throw new WebApplicationException(Status.NOT_FOUND);
		}
		
		JSONObject jsonEvent = new JSONObject(multiPart.getField("event").getValue());
		List<FormDataBodyPart> bodyParts = multiPart.getFields("photos");
		
		boolean wrongType = false;
		List<String> images = new ArrayList<>(); 
		for (int i = 0; i < bodyParts.size(); i++) {
			BodyPartEntity bodyPartEntity = (BodyPartEntity) bodyParts.get(i).getEntity();
			String type = bodyParts.get(i).getMediaType().toString();
			
			if(!type.contains("image")) {
				wrongType = true;
			}
			
			String base64 = new String(Base64.encodeBase64(IOUtils.toByteArray(bodyPartEntity.getInputStream())), "UTF-8");
			images.add("data:" + type + ";base64," + base64);
		}
		
		if(jsonEvent == null || wrongType) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		
		EventDto eventDto = EventDto.parse(jsonEvent);
		eventDto.setImages(images);
		
		List<String> errors = this.eventFacade.updateEvent(id, eventDto);

		if(!errors.isEmpty()) {
			return Response.status(Status.BAD_REQUEST).entity(errors).build();
		}
		
		return Response.ok(new JSONArray()).build();
	}

	@Path("/{id}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteEvent(@PathParam("id") long id) {
		EventDto event = this.eventFacade.findEvent(id);
		
		if(event == null) {
			throw new WebApplicationException(Status.NOT_FOUND);
		}

		List<String> errors = this.eventFacade.deleteEvent(id);
		
		if(!errors.isEmpty()) {
			return Response.status(Status.BAD_REQUEST).entity(errors).build();
		}
		
		return Response.ok(new JSONArray()).build();
	}

	@Path("/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public EventDto getEvent(@PathParam("id") long id) {
		EventDto dto = this.eventFacade.findEvent(id);
		
		if(dto == null) {
			throw new WebApplicationException(Status.NOT_FOUND);
		}
		
		return dto;
	}

	@Path("/")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<EventDto> getEvents() {
		return this.eventFacade.listEvents();
	}
}
