package br.com.turismoinf.web.interfaceapi.dto.parse;

import br.com.turismoinf.web.domain.models.user.UserClient;
import br.com.turismoinf.web.interfaceapi.dto.UserClientDto;

public class UserClientDtoParse {

	public UserClientDto parseModelToDto(UserClient user) {
		UserClientDto dto = new UserClientDto();
		
		dto.setId(user.getId());
		dto.setEmail(user.getEmail());
		dto.setName(user.getName());
		
		return dto;
	}
	
}
