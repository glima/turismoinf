package br.com.turismoinf.web.interfaceapi.facade.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;
import javax.inject.Inject;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import br.com.turismoinf.web.application.service.IEventService;
import br.com.turismoinf.web.domain.models.event.IEventRepository;
import br.com.turismoinf.web.domain.models.event.Event;
import br.com.turismoinf.web.infra.dao.impl.ClientDao;
import br.com.turismoinf.web.interfaceapi.dto.EventDto;
import br.com.turismoinf.web.interfaceapi.dto.PlaceDto;
import br.com.turismoinf.web.interfaceapi.dto.parse.EventDtoParse;
import br.com.turismoinf.web.interfaceapi.facade.IEventFacade;
import br.com.turismoinf.web.interfaceapi.facade.Result;

@Service
public class EventFacade implements IEventFacade {
	
	@Inject private IEventRepository eventRepository;
	@Inject private IEventService eventService;
	@Inject @Resource(name = "clientDao") private ClientDao clientDao;
	
	@Override
	public List<EventDto> listEvents(int page) {
		List<EventDto> outputs = this.clientDao.listEvents(page);
		
		for (EventDto dto : outputs) {
			if(!dto.getImages().isEmpty()) {
				dto.setImages(Arrays.asList(dto.getImages().get(0)));
			}
		}
		
		return outputs;
	}

	@Override
	public Result<EventDto> addEvent(EventDto eventDto) {
//		List<Long> ids = new ArrayList<>();
//		if(eventDto.getCategories() != null) {
//			eventDto.getCategories().forEach(item -> {
//				ids.add(item.getId());
//			});
//		}
		
		List<String> errors = this.eventService.canCreate(eventDto.getName(), eventDto.getDescription(), eventDto.getCep(), eventDto.getCity(),
				eventDto.getStreet(), eventDto.getNumber(), eventDto.getEmail(), eventDto.getPhone(), 
				eventDto.getDateStart(), eventDto.getDateEnd(), eventDto.getTimeStart(), eventDto.getTimeEnd(), eventDto.getPrice());
		
		if(errors.isEmpty()) {
			Event event = this.eventService.createEvent(eventDto.getName(), eventDto.getDescription(), eventDto.getCep(), eventDto.getCity(),
					eventDto.getStreet(), eventDto.getNumber(), eventDto.getImages(), eventDto.getEmail(), eventDto.getPhone(), 
					eventDto.getDateStart(), eventDto.getDateEnd(), eventDto.getTimeStart(), eventDto.getTimeEnd(), eventDto.getPrice());
			return new Result<EventDto>(new EventDtoParse().parseModelToDto(event));
		}
		
		return new Result<EventDto>(errors);
	}

	@Override
	public List<String> deleteEvent(long id) {
		List<String> errors = this.eventService.canDelete(id);
		
		if(errors.isEmpty()) {
			try {
				this.eventService.deleteEvent(id);
			} catch (DataIntegrityViolationException e) {
				errors.add("Local não pode ser excluído pois possui dados associados.");
			}
		}
		
		return errors;
	}

	@Override
	public List<String> updateEvent(long id, EventDto eventDto) {
//		List<Long> ids = new ArrayList<>();
//		if(eventDto.getCategories() != null) {
//			eventDto.getCategories().forEach(item -> {
//				ids.add(item.getId());
//			});
//		}
		
		List<String> errors = this.eventService.canUpdate(id, eventDto.getName(), eventDto.getDescription(), eventDto.getCep(), eventDto.getCity(),
				eventDto.getStreet(), eventDto.getNumber(), eventDto.getEmail(), eventDto.getPhone(), 
				eventDto.getDateStart(), eventDto.getDateEnd(), eventDto.getTimeStart(), eventDto.getTimeEnd(), eventDto.getPrice());
		
		if(errors.isEmpty()) {
			this.eventService.updateEvent(id, eventDto.getName(), eventDto.getDescription(), eventDto.getCep(), eventDto.getCity(),
					eventDto.getStreet(), eventDto.getNumber(), eventDto.getImages(), eventDto.getEmail(), eventDto.getPhone(), 
					eventDto.getDateStart(), eventDto.getDateEnd(), eventDto.getTimeStart(), eventDto.getTimeEnd(), eventDto.getPrice());
		}
		
		return errors;
	}

	@Override
	public EventDto findEvent(long id) {
		Event event = this.eventRepository.findOne(id);

		EventDto dto = null;
		if(event != null) {
			dto = new EventDtoParse().parseModelToDto(event);
		}
		
		return dto;
	}

	@Override
	public List<EventDto> listEvents() {
		List<Event> outputs = this.eventRepository.findAll();
		
		EventDtoParse dtoParse = new EventDtoParse();
		
		List<EventDto> dtos = new ArrayList<>();
		for (Event event : outputs) {
			dtos.add(dtoParse.parseModelToDtoMinified(event));
		}
		
		return dtos;
	}

	@Override
	public List<String> getPhotos(long id) {
		Event event = this.eventRepository.findOne(id);

		List<String> photos = new ArrayList<>();
		if(event != null) {
			event.getPhotos().forEach(photo -> {
				photos.add(photo.getId().toString());
			});
		}
		
		return photos;
	}

	@Override
	public String getEventPhoto(long id) {
		String image = this.clientDao.getEventImage(id);
		
		return image;
	}

}
