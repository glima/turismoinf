package br.com.turismoinf.web.interfaceapi.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class ListDto {

	private long id;
	private String description;
	
	public ListDto() {
	
	}
	
	public ListDto(long id, String description) {
		this.id = id;
		this.setDescription(description);
	}
	
	public ListDto(long id) {
		this.id = id;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
