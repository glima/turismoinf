package br.com.turismoinf.web.interfaceapi.facade;

import java.util.List;

import br.com.turismoinf.web.domain.models.user.User;
import br.com.turismoinf.web.interfaceapi.dto.UserDto;

public interface IUserFacade {

	public Result<UserDto> addUser(UserDto userDto);
	
	public List<String> deleteUser(long id);
	
	public List<String> updateUser(long id, UserDto userDto);
	
	public UserDto findUser(long id);
	
	public List<UserDto> listUsers();

	public List<String> resetUserPassword(User user, UserDto userDto);
	
	public List<String> updateStatusUser(long id, boolean status);

	public List<String> forgottenPassword(String email);
}
