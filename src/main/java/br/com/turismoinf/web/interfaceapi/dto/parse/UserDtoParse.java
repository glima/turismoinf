package br.com.turismoinf.web.interfaceapi.dto.parse;

import br.com.turismoinf.web.domain.models.user.User;
import br.com.turismoinf.web.interfaceapi.dto.UserDto;

public class UserDtoParse {

	public UserDto parseModelToDto(User user) {
		UserDto dto = new UserDto();
		
		dto.setId(user.getId());
		dto.setEmail(user.getEmail());
		dto.setName(user.getName());
		dto.setLogin(user.getLogin());
		dto.setStatus(user.isStatus());
		
		return dto;
	}
	
	public UserDto parseModelToDtoMinified(User user) {
		UserDto dto = new UserDto();
		
		dto.setId(user.getId());
		dto.setName(user.getName());
		dto.setStatus(user.isStatus());

		return dto;
	}
	
}
