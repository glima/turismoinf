package br.com.turismoinf.web.interfaceapi.facade.impl;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.mail.MessagingException;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.turismoinf.web.application.service.IUserService;
import br.com.turismoinf.web.domain.models.user.IUserRepository;
import br.com.turismoinf.web.domain.models.user.User;
import br.com.turismoinf.web.infra.dao.impl.ConfigurationDao;
import br.com.turismoinf.web.interfaceapi.dto.UserDto;
import br.com.turismoinf.web.interfaceapi.dto.parse.UserDtoParse;
import br.com.turismoinf.web.interfaceapi.facade.IUserFacade;
import br.com.turismoinf.web.interfaceapi.facade.Result;
import br.com.turismoinf.web.mail.ResetPasswordMail;
import br.com.turismoinf.web.utils.EmailUtil;

@Service
public class UserFacade implements IUserFacade {

	@Inject IUserRepository userRepository;
	@Inject IUserService userService;
	@Inject @Resource(name="confDao") private ConfigurationDao confDao;
	
	public Result<UserDto> addUser(UserDto userDto) {
		String login = this.extractLoginFromEmail(userDto.getEmail());
		List<String> errors = this.userService.canCreate(userDto.getName(), login, userDto.getPassword(), 
				userDto.getEmail());
		
		if(!errors.isEmpty()) {
			return new Result<UserDto>(errors);
		}
		
		User user = this.userService.createUser(userDto.getName(), login, userDto.getPassword(), 
				userDto.getEmail());
		
		return new Result<UserDto>(new UserDtoParse().parseModelToDto(user));
	}	
	
	private String extractLoginFromEmail(String email) {
		if(email != null) {
			String[] split = email.split("@");
			
			return split[0];
		}
		
		return null;
	}

	public List<String> deleteUser(long id) {
		List<String> errors = this.userService.canDelete(id);
		
		if(errors.isEmpty()) {
			try {
				this.userService.deleteUser(id);
			} catch (DataIntegrityViolationException e) {
				errors.add("Usuário não pode ser excluído pois possui dados associados.");
			}
		}
		
		return errors;
	}

	public List<String> updateUser(long id, UserDto userDto) {
		String login = this.extractLoginFromEmail(userDto.getEmail());
		List<String> errors = this.userService.canUpdate(id, userDto.getName(), login, userDto.getPassword(), 
				userDto.getEmail());
		
		if(errors.isEmpty()) {
			this.userService.updateUser(id, userDto.getName(), login, userDto.getPassword(), 
				userDto.getEmail());
		}
		
		return errors;
	}

	public UserDto findUser(long id) {
		User user = this.userRepository.findById(id);
		
		UserDto dto = null;
		if(user != null) {
			dto = new UserDtoParse().parseModelToDto(user);
		}
		
		return dto;
	}

	public List<UserDto> listUsers() {
		List<User> users = this.userRepository.findAll();
		
		UserDtoParse dtoParse = new UserDtoParse();
		List<UserDto> usersDto = new ArrayList<UserDto>();
		for (User user : users) {
			usersDto.add(dtoParse.parseModelToDto(user));
		}
		
		return usersDto;
	}

	public UserDto getUser(User userContext) {
		User user = this.userRepository.findByUsername(userContext.getLogin());
		
		UserDto dto = null;
		if(user != null) {
			dto = new UserDtoParse().parseModelToDtoMinified(user);
		}
		
		return dto;
	}

	public List<String> resetUserPassword(User userContext, UserDto userDto) {
		UserDto user = (UserDto) getUser(userContext);
		List<String> errors = this.userService.canResetPassword(user.getId(), userDto.getPassword(), userDto.getNewPassword());
		
		if(errors.isEmpty()) {
			this.userService.resetPassword(user.getId(), userDto.getPassword(), userDto.getNewPassword());
		}
		
		return errors;
	}
	

	public List<String> updateStatusUser(long id, boolean status) {
		List<String> errors = null;
		if(status) {
			errors = this.userService.canUpdateStatus(id);
			
			if(errors.isEmpty()) {
				this.userService.enableUser(id);
			}
		} else {
			errors = this.userService.canUpdateStatus(id);
			
			if(errors.isEmpty()) {
				this.userService.disableUser(id);
			}
		}
		
		return errors;
	}

	@Override
	public List<String> forgottenPassword(String email) {
		List<String> errors = new ArrayList<>();
		
		boolean thereIsEmailConf = this.confDao.thereIsEmailConf();
		if(thereIsEmailConf) {
			User user = this.userRepository.findByEmail(email);
			if(user == null) {
				errors.add("Email não cadastrado.");
			} else {
				Md5PasswordEncoder encoder = new Md5PasswordEncoder();
				String newpass = encoder.encodePassword(email + "turismo", null);
				
				user.setPassword(newpass);
				
				List<String> address = new ArrayList<String>();
				address.add(email);
				
				try {
					Map<String, Object> content = new HashMap<String, Object>();
					content.put(ResetPasswordMail.USERNAME, user.getName());
					content.put(ResetPasswordMail.NEW_PASS, newpass);

					EmailUtil.envia(EmailUtil.getConfigEmail(), "Redefinição de senha", content, address, new ResetPasswordMail());
					
					this.userRepository.save(user);
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				} catch (MessagingException e) {
					e.printStackTrace();
				}
			}
		} else {
			errors.add("Não foi possível enviar um email para redefinição.");
		}
		
		return errors;
	}

}
