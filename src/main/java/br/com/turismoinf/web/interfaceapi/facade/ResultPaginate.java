package br.com.turismoinf.web.interfaceapi.facade;

import java.util.List;

public class ResultPaginate<DTO> {

	private List<DTO> list;
	private long total;
	
	public ResultPaginate(List<DTO> list, long total) {
		this.list = list;
		this.total = total;
	}
	
	public List<DTO> getList() {
		return list;
	}
	
	public void setList(List<DTO> list) {
		this.list = list;
	}
	
	public long getTotal() {
		return total;
	}
	
	public void setTotal(long total) {
		this.total = total;
	}
	
	
}
