package br.com.turismoinf.web.interfaceapi.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.media.multipart.BodyPartEntity;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import br.com.turismoinf.web.interfaceapi.dto.PlaceDto;
import br.com.turismoinf.web.interfaceapi.facade.IPlaceFacade;
import br.com.turismoinf.web.interfaceapi.facade.Result;

@Component
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
@Path("/adm/places")
public class PlaceController {

	@Inject private IPlaceFacade placeFacade;
	
	@Path("/")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response addPlace(final FormDataMultiPart multiPart) throws IOException {
		JSONObject jsonPlace = new JSONObject(multiPart.getField("place").getValue());
		List<FormDataBodyPart> bodyParts = multiPart.getFields("photos");
		
		boolean wrongType = false;
		List<String> images = new ArrayList<>(); 
		for (int i = 0; i < bodyParts.size(); i++) {
			BodyPartEntity bodyPartEntity = (BodyPartEntity) bodyParts.get(i).getEntity();
			String type = bodyParts.get(i).getMediaType().toString();
			
			if(!type.contains("image")) {
				wrongType = true;
			}
						
			String base64 = new String(Base64.encodeBase64(IOUtils.toByteArray(bodyPartEntity.getInputStream())), "UTF-8");
			images.add("data:" + type + ";base64," + base64);
		}
		
		if(jsonPlace == null || wrongType) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		
		PlaceDto placeDto = PlaceDto.parse(jsonPlace);
		placeDto.setImages(images);
		Result<PlaceDto> result = this.placeFacade.addPlace(placeDto);
		
		if(!result.getErrors().isEmpty()) {
			return Response.status(Status.BAD_REQUEST).entity(result.getErrors()).build();
		}
		
		return Response.ok(result.getObject()).build();
	}

	@Path("/{id}")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response updatePlace(@PathParam("id") long id, final FormDataMultiPart multiPart) throws IOException {
		PlaceDto place = this.placeFacade.findPlace(id);
		
		if(place == null) {
			throw new WebApplicationException(Status.NOT_FOUND);
		}
		
		JSONObject jsonPlace = new JSONObject(multiPart.getField("place").getValue());
		List<FormDataBodyPart> bodyParts = multiPart.getFields("photos");
		
		boolean wrongType = false;
		List<String> images = new ArrayList<>(); 
		for (int i = 0; i < bodyParts.size(); i++) {
			BodyPartEntity bodyPartEntity = (BodyPartEntity) bodyParts.get(i).getEntity();
			String type = bodyParts.get(i).getMediaType().toString();
			
			if(!type.contains("image")) {
				wrongType = true;
			}
			
			String base64 = new String(Base64.encodeBase64(IOUtils.toByteArray(bodyPartEntity.getInputStream())), "UTF-8");
			images.add("data:" + type + ";base64," + base64);
		}
		
		if(jsonPlace == null || wrongType) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		
		PlaceDto placeDto = PlaceDto.parse(jsonPlace);
		placeDto.setImages(images);
		
		List<String> errors = this.placeFacade.updatePlace(id, placeDto);

		if(!errors.isEmpty()) {
			return Response.status(Status.BAD_REQUEST).entity(errors).build();
		}
		
		return Response.ok(new JSONArray()).build();
	}

	@Path("/{id}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response deletePlace(@PathParam("id") long id) {
		PlaceDto place = this.placeFacade.findPlace(id);
		
		if(place == null) {
			throw new WebApplicationException(Status.NOT_FOUND);
		}

		List<String> errors = this.placeFacade.deletePlace(id);
		
		if(!errors.isEmpty()) {
			return Response.status(Status.BAD_REQUEST).entity(errors).build();
		}
		
		return Response.ok(new JSONArray()).build();
	}

	@Path("/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public PlaceDto getPlace(@PathParam("id") long id) {
		PlaceDto dto = this.placeFacade.findPlace(id);
		
		if(dto == null) {
			throw new WebApplicationException(Status.NOT_FOUND);
		}
		
		return dto;
	}

	@Path("/")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<PlaceDto> getPlaces() {
		return this.placeFacade.listPlaces();
	}
}
