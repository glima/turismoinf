package br.com.turismoinf.web.interfaceapi.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.media.multipart.BodyPartEntity;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import br.com.turismoinf.web.interfaceapi.dto.CategoryDto;
import br.com.turismoinf.web.interfaceapi.facade.ICategoryFacade;
import br.com.turismoinf.web.interfaceapi.facade.Result;

@Component
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
@Path("/adm/categories")
public class CategoryController {

	@Inject private ICategoryFacade categoryFacade;
	
	@Path("/")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response updateConf(final FormDataMultiPart multiPart) throws UnsupportedEncodingException, IOException {
		JSONObject jsonCategory = new JSONObject(multiPart.getField("category").getValue());
		FormDataBodyPart bodyParts = multiPart.getField("icon");
		
		boolean wrongType = false;
		String image = null;
		
		if(bodyParts != null) {
			BodyPartEntity bodyPartEntity = (BodyPartEntity) bodyParts.getEntity();
			String type = bodyParts.getMediaType().toString();
			
			if(!type.contains("image")) {
				wrongType = true;
			}
						
			String base64 = new String(Base64.encodeBase64(IOUtils.toByteArray(bodyPartEntity.getInputStream())), "UTF-8");
			image = ("data:" + type + ";base64," + base64);
		}
		
		if(jsonCategory == null || wrongType) {
			return Response.status(Status.BAD_REQUEST).entity("Dados informados inválidos.").build();
		}
		
		CategoryDto categoryDto = CategoryDto.parse(jsonCategory);
		categoryDto.setImage(image);
		Result<CategoryDto> result = this.categoryFacade.addCategory(categoryDto);
		
		if(!result.getErrors().isEmpty()) {
			return Response.status(Status.BAD_REQUEST).entity(result.getErrors()).build();
		}
		
		return Response.ok(result.getObject()).build();
	}
	
	@Path("/{id}")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response updatePlace(@PathParam("id") long id, final FormDataMultiPart multiPart) throws IOException {
		CategoryDto category = this.categoryFacade.findCategory(id);
		
		if(category == null) {
			throw new WebApplicationException(Status.NOT_FOUND);
		}
		
		JSONObject jsonCategory = new JSONObject(multiPart.getField("category").getValue());
		FormDataBodyPart bodyParts = multiPart.getField("icon");
		
		boolean wrongType = false;
		String image = null;
		
		if(bodyParts != null) {
			BodyPartEntity bodyPartEntity = (BodyPartEntity) bodyParts.getEntity();
			String type = bodyParts.getMediaType().toString();
			
			if(!type.contains("image")) {
				wrongType = true;
			}
						
			String base64 = new String(Base64.encodeBase64(IOUtils.toByteArray(bodyPartEntity.getInputStream())), "UTF-8");
			image = ("data:" + type + ";base64," + base64);
		}
		
		if(jsonCategory == null || wrongType) {
			return Response.status(Status.BAD_REQUEST).entity("Dados informados inválidos.").build();
		}
		
		CategoryDto categoryDto = CategoryDto.parse(jsonCategory);
		categoryDto.setImage(image);
		
		List<String> errors = this.categoryFacade.updateCategory(id, categoryDto);

		if(!errors.isEmpty()) {
			return Response.status(Status.BAD_REQUEST).entity(errors).build();
		}
		
		return Response.ok(new JSONArray()).build();
	}
	
	/*@Path("/")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addCategory(CategoryDto categoryDto) {
		Result<CategoryDto> result = this.categoryFacade.addCategory(categoryDto);
		
		if(!result.getErrors().isEmpty()) {
			return Response.status(Status.BAD_REQUEST).entity(result.getErrors()).build();
		}
		
		return Response.ok(result.getObject()).build();
	}*/

	/*@Path("/{id}")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateCategory(@PathParam("id") long id, CategoryDto categoryDto) {
		CategoryDto category = this.categoryFacade.findCategory(id);
		
		if(category == null) {
			throw new WebApplicationException(Status.NOT_FOUND);
		}
		
		List<String> errors = this.categoryFacade.updateCategory(id, categoryDto);

		if(!errors.isEmpty()) {
			return Response.status(Status.BAD_REQUEST).entity(errors).build();
		}
		
		return Response.ok(new JSONArray()).build();
	}*/

	@Path("/{id}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteCategory(@PathParam("id") long id) {
		CategoryDto category = this.categoryFacade.findCategory(id);
		
		if(category == null) {
			throw new WebApplicationException(Status.NOT_FOUND);
		}

		List<String> errors = this.categoryFacade.deleteCategory(id);
		
		if(!errors.isEmpty()) {
			return Response.status(Status.BAD_REQUEST).entity(errors).build();
		}
		
		return Response.ok(new JSONArray()).build();
	}

	@Path("/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public CategoryDto getCategory(@PathParam("id") long id) {
		CategoryDto dto = this.categoryFacade.findCategory(id);
		
		if(dto == null) {
			throw new WebApplicationException(Status.NOT_FOUND);
		}
		
		return dto;
	}

	@Path("/")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<CategoryDto> getCategorys() {
		return this.categoryFacade.listCategories();
	}
}
