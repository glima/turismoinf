package br.com.turismoinf.web.interfaceapi.facade;

import java.util.List;

import br.com.turismoinf.web.interfaceapi.dto.CategoryDto;
import br.com.turismoinf.web.interfaceapi.dto.PlaceDto;

public interface ICategoryFacade {

	public Result<CategoryDto> addCategory(CategoryDto categoryDto);
	
	public List<String> deleteCategory(long id);
	
	public List<String> updateCategory(long id, CategoryDto categoryDto);
	
	public CategoryDto findCategory(long id);
	
	public List<CategoryDto> listCategories();
	
	public List<CategoryDto> listCategories(int page);

	public List<PlaceDto> listPlaces(long id, int page);

	public String getCategoryPhoto(long id);
}
	