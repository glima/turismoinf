package br.com.turismoinf.web.interfaceapi.dto;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class ConfigurationDto {

	private String email;
	private String password;
	private Integer port;
	private String server;
	private String description;
	private String image;
	private Long phoneContact;
	private String emailContact;
	
	public ConfigurationDto() {
	
	}

	public static ConfigurationDto parse(JSONObject json) {
		ConfigurationDto dto = new ConfigurationDto();
		
		if(json.length() > 0) {
			if(json.has("description"))
				dto.setDescription(json.getString("description"));
			
			if(json.has("email"))
				dto.setEmail(json.getString("email"));
			
			if(json.has("password"))
				dto.setPassword(json.getString("password"));
			
			if(json.has("server"))
				dto.setServer(json.getString("server"));
			
			if(json.has("port"))
				dto.setPort(json.getString("port").equals("") ? null : json.getInt("port"));
			
			if(json.has("emailContact"))
				dto.setEmailContact(json.getString("emailContact"));
			
			if(json.has("phoneContact"))
				dto.setPhoneContact(json.getString("phoneContact").equals("") ? null : json.getLong("phoneContact"));
		}
		
		return dto;
	}
	
	public boolean hasEmailConfValue() {
		if(StringUtils.isBlank(email)
			&& StringUtils.isBlank(password)
			&& StringUtils.isBlank(server)
			&& (port == null || port == 0)) {
			return false;
		}
		
		return true;
	}
	
	public boolean hasInitialConfValue() {
		if(StringUtils.isBlank(description) && StringUtils.isBlank(image)) {
			return false;
		}
		
		return true;
	}
	
	public boolean hasContactConfValue() {
		if(StringUtils.isBlank(emailContact) && (phoneContact == null || phoneContact.equals(0))) {
			return false;
		}
		
		return true;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Long getPhoneContact() {
		return phoneContact;
	}

	public void setPhoneContact(Long phoneContact) {
		this.phoneContact = phoneContact;
	}

	public String getEmailContact() {
		return emailContact;
	}

	public void setEmailContact(String emailContact) {
		this.emailContact = emailContact;
	}
	
	
}
