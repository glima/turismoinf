package br.com.turismoinf.web.interfaceapi.facade;

import java.util.List;

import br.com.turismoinf.web.interfaceapi.dto.UserClientDto;
import br.com.turismoinf.web.security.model.UserContext;

public interface IUserClientFacade {

	public Result<UserClientDto> addUserClient(UserClientDto userClientDto);

	public List<String> forgottenPassword(String email);

	public List<String> resetUserPassword(UserContext user, UserClientDto userClientDto);
	
}
