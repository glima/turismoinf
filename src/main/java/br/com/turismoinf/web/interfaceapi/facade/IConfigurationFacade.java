package br.com.turismoinf.web.interfaceapi.facade;

import br.com.turismoinf.web.interfaceapi.dto.ConfigurationDto;

public interface IConfigurationFacade {

	public Result<ConfigurationDto> updateConfiguration(ConfigurationDto dto);
	
	public ConfigurationDto getConfiguration();
	
	public ConfigurationDto getInitialConfiguration();
	
	public ConfigurationDto getContactConfiguration();
}
