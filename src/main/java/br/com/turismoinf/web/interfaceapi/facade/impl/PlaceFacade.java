package br.com.turismoinf.web.interfaceapi.facade.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.inject.Inject;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import br.com.turismoinf.web.application.service.IPlaceService;
import br.com.turismoinf.web.domain.models.place.IPlaceRepository;
import br.com.turismoinf.web.domain.models.place.Place;
import br.com.turismoinf.web.infra.dao.impl.ClientDao;
import br.com.turismoinf.web.interfaceapi.dto.PlaceDto;
import br.com.turismoinf.web.interfaceapi.dto.parse.PlaceDtoParse;
import br.com.turismoinf.web.interfaceapi.facade.IPlaceFacade;
import br.com.turismoinf.web.interfaceapi.facade.Result;

@Service
public class PlaceFacade implements IPlaceFacade {
	
	@Inject private IPlaceRepository placeRepository;
	@Inject private IPlaceService placeService;
	@Inject @Resource(name = "clientDao") private ClientDao clientDao;

	@Override
	public Result<PlaceDto> addPlace(PlaceDto placeDto) {
		List<Long> ids = new ArrayList<>();
		if(placeDto.getCategories() != null) {
			placeDto.getCategories().forEach(item -> {
				ids.add(item.getId());
			});
		}
		
		List<String> errors = this.placeService.canCreate(placeDto.getName(), placeDto.getDescription(), placeDto.getCep(), placeDto.getCity(),
				placeDto.getStreet(), placeDto.getNumber(), ids, placeDto.getEmail(), placeDto.getPhone());
		
		if(errors.isEmpty()) {
			Place place = this.placeService.createPlace(placeDto.getName(), placeDto.getDescription(), placeDto.getCep(), placeDto.getCity(),
					placeDto.getStreet(), placeDto.getNumber(), ids, placeDto.getImages(), placeDto.getEmail(), placeDto.getPhone());
			return new Result<PlaceDto>(new PlaceDtoParse().parseModelToDto(place));
		}
		
		return new Result<PlaceDto>(errors);
	}

	@Override
	public List<String> deletePlace(long id) {
		List<String> errors = this.placeService.canDelete(id);
		
		if(errors.isEmpty()) {
			try {
				this.placeService.deletePlace(id);
			} catch (DataIntegrityViolationException e) {
				errors.add("Local não pode ser excluído pois possui dados associados.");
			}
		}
		
		return errors;
	}

	@Override
	public List<String> updatePlace(long id, PlaceDto placeDto) {
		List<Long> ids = new ArrayList<>();
		if(placeDto.getCategories() != null) {
			placeDto.getCategories().forEach(item -> {
				ids.add(item.getId());
			});
		}
		
		List<String> errors = this.placeService.canUpdate(id, placeDto.getName(), placeDto.getDescription(), placeDto.getCep(), placeDto.getCity(),
				placeDto.getStreet(), placeDto.getNumber(), ids, placeDto.getEmail(), placeDto.getPhone());
		
		if(errors.isEmpty()) {
			this.placeService.updatePlace(id, placeDto.getName(), placeDto.getDescription(), placeDto.getCep(), placeDto.getCity(),
					placeDto.getStreet(), placeDto.getNumber(), ids, placeDto.getImages(), placeDto.getEmail(), placeDto.getPhone());
		}
		
		return errors;
	}

	@Override
	public PlaceDto findPlace(long id) {
		Place place = this.placeRepository.findOne(id);

		PlaceDto dto = null;
		if(place != null) {
			dto = new PlaceDtoParse().parseModelToDto(place);
		}
		
		return dto;
	}

	@Override
	public List<PlaceDto> listPlaces() {
		List<Place> outputs = this.placeRepository.findAll();
		
		PlaceDtoParse dtoParse = new PlaceDtoParse();
		
		List<PlaceDto> dtos = new ArrayList<>();
		for (Place place : outputs) {
			dtos.add(dtoParse.parseModelToDtoMinified(place));
		}
		
		return dtos;
	}

	@Override
	public List<String> getPhotos(long id) {
		Place place = this.placeRepository.findOne(id);

		List<String> photos = new ArrayList<>();
		if(place != null) {
			place.getPhotos().forEach(photo -> {
				photos.add(photo.getId().toString());
			});
		}
		
		return photos;
	}

	@Override
	public String getPlacePhoto(long id) {
		String image = this.clientDao.getPlaceImage(id);
		
		return image;
	}

}
