package br.com.turismoinf.web.interfaceapi.controller;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.binary.Base64;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import br.com.turismoinf.web.interfaceapi.dto.CategoryDto;
import br.com.turismoinf.web.interfaceapi.dto.ConfigurationDto;
import br.com.turismoinf.web.interfaceapi.dto.EventDto;
import br.com.turismoinf.web.interfaceapi.dto.PlaceDto;
import br.com.turismoinf.web.interfaceapi.dto.UserClientDto;
import br.com.turismoinf.web.interfaceapi.facade.ICategoryFacade;
import br.com.turismoinf.web.interfaceapi.facade.IConfigurationFacade;
import br.com.turismoinf.web.interfaceapi.facade.IEventFacade;
import br.com.turismoinf.web.interfaceapi.facade.IPlaceFacade;
import br.com.turismoinf.web.interfaceapi.facade.IUserClientFacade;
import br.com.turismoinf.web.interfaceapi.facade.Result;
import br.com.turismoinf.web.security.model.UserContext;
import br.com.turismoinf.web.utils.EmailUtil;

@Component
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
@Path("/client")
public class ClientController {
	
	@Inject private IUserClientFacade userFacade;
	@Inject private ICategoryFacade categoryFacade;
	@Inject private IPlaceFacade placeFacade;
	@Inject private IEventFacade eventFacade;
	@Inject private IConfigurationFacade configurationFacade;
	
	@PUT
	@Path("/user/forgotten_password")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response restauraSenha(UserClientDto userDto) {
		
		if(userDto == null || userDto.getEmail() == null || EmailUtil.isValidateEmail(userDto.getEmail()) == false) {
			return Response.status(Status.BAD_REQUEST).entity("Email inválido.").build();
		}
		
		List<String> errors = this.userFacade.forgottenPassword(userDto.getEmail());
		if(!errors.isEmpty()) {
			return Response.status(Status.BAD_REQUEST).entity(errors.get(0)).build();
		}
		
		return Response.ok("Foi encaminhado um email para o usuário cadastrado.").build();
	}
	
	private UserContext getUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return (UserContext) authentication.getPrincipal();
	}
	
	@Path("/user/password")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateUserPassword(UserClientDto userDto) {
		List<String> errors = this.userFacade.resetUserPassword(getUser(), userDto);

		if(!errors.isEmpty()) {
			return Response.status(Status.BAD_REQUEST).entity(errors).build();
		}
		
		return Response.ok("Senha alterada com sucesso.").build();
	}
	
	@Path("/user")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addUserClient(UserClientDto userDto) {
		Result<UserClientDto> result = this.userFacade.addUserClient(userDto);
		
		if(!result.getErrors().isEmpty()) {
			return Response.status(Status.BAD_REQUEST).entity(result.getErrors()).build();
		}
		
		return Response.ok(result.getObject()).build();
	}
	
	@Path("/categories")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<CategoryDto> getCategorys() {
		return this.categoryFacade.listCategories();
	}
	
	@Path("/config/initial")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ConfigurationDto getConfig() {
		return this.configurationFacade.getInitialConfiguration();
	}
	
	@Path("/config/contact")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ConfigurationDto getContactConfig() {
		return this.configurationFacade.getContactConfiguration();
	}
	
	@Path("/events")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<EventDto> getEvents(@QueryParam("page") int page) {
		List<EventDto> events = this.eventFacade.listEvents(page);
		
		return events;
	}
	
	@Path("/categories/{id}/places")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<PlaceDto> getPlaces(@PathParam("id") long id, @QueryParam("page") int page) {
		CategoryDto dto = this.categoryFacade.findCategory(id);
		
		if(dto == null) {
			throw new WebApplicationException(Status.NOT_FOUND);
		}
		
		List<PlaceDto> places = this.categoryFacade.listPlaces(id, page);
		
		return places;
	}
	
	@Path("/categories/photo/{id}")
	@GET
	@Produces(value = { "image/png", "image/jpg", "image/jpeg", "image/gif" })
	public Response getCategoryPhoto(@PathParam("id") long id) {
		String photo = this.categoryFacade.getCategoryPhoto(id);
		
		if(photo == null) {
			throw new WebApplicationException(Status.NOT_FOUND);
		}
		
		String[] split = photo.split(";");
		return Response.ok(Base64.decodeBase64(split[1].replaceAll("base64,", "")), split[0].split(":")[1]).build();
		
	}
	
	@Path("/places/{id}/photos")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<String> getPlacesPhotos(@PathParam("id") long id) {
		PlaceDto dto = this.placeFacade.findPlace(id);
		if(dto == null) {
			throw new WebApplicationException(Status.NOT_FOUND);
		}
		
		return this.placeFacade.getPhotos(id);
	}
	
	@Path("/events/{id}/photos")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<String> getEventsPhotos(@PathParam("id") long id) {
		EventDto dto = this.eventFacade.findEvent(id);
		if(dto == null) {
			throw new WebApplicationException(Status.NOT_FOUND);
		}
		
		return this.eventFacade.getPhotos(id);
	}
	
	@Path("/places/photos/{id}")
	@GET
	@Produces(value = { "image/png", "image/jpg", "image/jpeg", "image/gif" })
	public Response getPlacesPhoto(@PathParam("id") long id) {
		String photo = this.placeFacade.getPlacePhoto(id);
		
		if(photo == null) {
			throw new WebApplicationException(Status.NOT_FOUND);
		}
		
		String[] split = photo.split(";");
		return Response.ok(Base64.decodeBase64(split[1].replaceAll("base64,", "")), split[0].split(":")[1]).build();
		
	}
	
	@Path("/events/photos/{id}")
	@GET
	@Produces(value = { "image/png", "image/jpg", "image/jpeg", "image/gif" })
	public Response getEventsPhoto(@PathParam("id") long id) {
		String photo = this.eventFacade.getEventPhoto(id);
		
		if(photo == null) {
			throw new WebApplicationException(Status.NOT_FOUND);
		}
		
		String[] split = photo.split(";");
		return Response.ok(Base64.decodeBase64(split[1].replaceAll("base64,", "")), split[0].split(":")[1]).build();
		
	}
}
