package br.com.turismoinf.web.interfaceapi.dto;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class PlaceDto {

	private long id;
	private String name;
	private String description;
	private int cep;
	private String city;
	private String street;
	private int number;
	private List<ListDto> categories;
	private List<String> images;
	private List<String> photos;
	
	private Long phone;
	private String email;
	
	public static PlaceDto parse(JSONObject json) {
		PlaceDto dto = new PlaceDto();
		
		dto.setName(json.has("name") ? json.getString("name") : null);
		dto.setDescription(json.has("description") ? json.getString("description") : null);
		dto.setCep(json.has("cep") ? json.getInt("cep") : null);
		dto.setCity(json.has("city") ? json.getString("city") : null);
		dto.setStreet(json.has("street") ? json.getString("street") : null);
		dto.setNumber(json.has("number") ? json.getInt("number") : null);
		dto.setPhone(json.getString("phone").equals("") ? null : json.getLong("phone"));
		dto.setEmail(json.getString("email"));
		
		JSONArray categories = json.has("categories") ? json.getJSONArray("categories") : new JSONArray();
		List<ListDto> list = new ArrayList<>();
		if(categories != null) {
			for (int i = 0; i < categories.length(); i++) {
				list.add(new ListDto(categories.getJSONObject(i).getLong("id"), categories.getJSONObject(i).getString("description")));
			}
		}
		
		dto.setCategories(list);
		
		return dto;
	}
	
	public void addCategory(ListDto dto) {
		this.categories.add(dto);
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public int getCep() {
		return cep;
	}
	
	public void setCep(int cep) {
		this.cep = cep;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getStreet() {
		return street;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
	
	public int getNumber() {
		return number;
	}
	
	public void setNumber(int number) {
		this.number = number;
	}

	public List<ListDto> getCategories() {
		return categories;
	}

	public void setCategories(List<ListDto> categories) {
		this.categories = categories;
	}

	public List<String> getImages() {
		return images;
	}

	public void setImages(List<String> images) {
		this.images = images;
	}

	public List<String> getPhotos() {
		return photos;
	}

	public void setPhotos(List<String> photos) {
		this.photos = photos;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getPhone() {
		return phone;
	}

	public void setPhone(Long phone) {
		this.phone = phone;
	}
	
	
}
