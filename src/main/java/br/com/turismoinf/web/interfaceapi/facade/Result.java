package br.com.turismoinf.web.interfaceapi.facade;

import java.util.ArrayList;
import java.util.List;

public class Result<DTO> {

	private List<String> errors;

	private DTO object;

    public Result(DTO dto) {
        this.object = dto;
        this.errors = new ArrayList<String>();
    }

    public Result(List<String> errors) {
        this.errors = errors;
    }

    public Result() {
        errors = new ArrayList<String>();
    }
    
    public List<String> getErrors() {
		return errors;
	}
    
    public DTO getObject() {
		return object;
	}
}
