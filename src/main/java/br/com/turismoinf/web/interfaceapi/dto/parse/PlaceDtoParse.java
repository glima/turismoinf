package br.com.turismoinf.web.interfaceapi.dto.parse;

import java.util.ArrayList;
import java.util.List;

import br.com.turismoinf.web.domain.models.place.Place;
import br.com.turismoinf.web.interfaceapi.dto.ListDto;
import br.com.turismoinf.web.interfaceapi.dto.PlaceDto;

public class PlaceDtoParse {

	public PlaceDto parseModelToDto(Place place) {
		PlaceDto dto = new PlaceDto();
		
		dto.setId(place.getId());
		dto.setDescription(place.getDescription());
		dto.setCep(place.getCep());
		dto.setCity(place.getCity());
		dto.setNumber(place.getNumber());
		dto.setStreet(place.getStreet());
		dto.setName(place.getName());
		dto.setEmail(place.getEmail());
		dto.setPhone(place.getPhone());
		
		List<ListDto> categories = new ArrayList<>();
		place.getCategories().forEach(item -> {
			categories.add(new ListDto(item.getId(), item.getDescription()));
		});
		
		dto.setCategories(categories);
		
		List<String> photos = new ArrayList<>();
		place.getPhotos().forEach(item -> {
			photos.add(item.getImage());
		});
		
		dto.setImages(photos);
		
		return dto;
	}
	
	public PlaceDto parseModelToDtoMinified(Place place) {
		PlaceDto dto = new PlaceDto();
		
		dto.setId(place.getId());
		dto.setName(place.getName());		
		
		return dto;
	}
	
	public PlaceDto parseModelToDtoMinifiedPhoto(Place place) {
		PlaceDto dto = new PlaceDto();
		
		dto.setId(place.getId());
		dto.setDescription(place.getDescription());
		dto.setCep(place.getCep());
		dto.setCity(place.getCity());
		dto.setNumber(place.getNumber());
		dto.setStreet(place.getStreet());
		dto.setName(place.getName());
		
		List<ListDto> categories = new ArrayList<>();
		place.getCategories().forEach(item -> {
			categories.add(new ListDto(item.getId(), item.getDescription()));
		});
		
		dto.setCategories(categories);
		
		List<String> photos = new ArrayList<>();
		if(!place.getPhotos().isEmpty()) {
			photos.add(new ArrayList<>(place.getPhotos()).get(0).getImage());
		}
		
		dto.setImages(photos);
		
		return dto;
	}
	
}
