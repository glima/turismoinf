package br.com.turismoinf.web.interfaceapi.controller;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import br.com.turismoinf.web.domain.models.user.User;
import br.com.turismoinf.web.interfaceapi.dto.UserDto;
import br.com.turismoinf.web.interfaceapi.facade.IUserFacade;
import br.com.turismoinf.web.interfaceapi.facade.Result;
import br.com.turismoinf.web.utils.EmailUtil;

@Component
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
@Path("/adm/user")
public class UserController {
	
	@Inject private IUserFacade userFacade;
	
	@PUT
	@Path("forgotten_password")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response restauraSenha(UserDto userDto) {
		
		if(userDto == null || userDto.getEmail() == null || EmailUtil.isValidateEmail(userDto.getEmail()) == false) {
			return Response.status(Status.BAD_REQUEST).entity("Email inválido.").build();
		}
		
		List<String> errors = this.userFacade.forgottenPassword(userDto.getEmail());
		if(!errors.isEmpty()) {
			return Response.status(Status.BAD_REQUEST).entity(errors.get(0)).build();
		}
		
		return Response.ok("Foi encaminhado um email para o usuário cadastrado.").build();
	}
	
	@Path("/{id}/status")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateStatusUser(@PathParam("id") long id, UserDto userDto) {
		UserDto user = this.userFacade.findUser(id);
		if(user == null) {
			throw new WebApplicationException(Status.NOT_FOUND);
		}
		
		List<String> errors = this.userFacade.updateStatusUser(id, userDto.isStatus());
		
		if(!errors.isEmpty()) {
			return Response.status(Status.BAD_REQUEST).entity(errors).build();
		}
		
		return Response.ok(new JSONArray()).build();
	}

	@Path("/")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addUser(UserDto userDto) {
		Result<UserDto> result = this.userFacade.addUser(userDto);
		
		if(!result.getErrors().isEmpty()) {
			return Response.status(Status.BAD_REQUEST).entity(result.getErrors()).build();
		}
		
		return Response.ok(result.getObject()).build();
	}
	
	@Path("/{id}")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateUser(@PathParam("id") long id, UserDto userDto) {
		UserDto user = this.userFacade.findUser(id);
		if(user == null) {
			throw new WebApplicationException(Status.NOT_FOUND);
		}
		
		List<String> errors = this.userFacade.updateUser(id, userDto);

		if(!errors.isEmpty()) {
			return Response.status(Status.BAD_REQUEST).entity(errors).build();
		}
		
		return Response.ok(new JSONArray()).build();
	}
	
	@Path("/password")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateUserPassword(UserDto userDto) {
		List<String> errors = this.userFacade.resetUserPassword(getUser(), userDto);

		if(!errors.isEmpty()) {
			return Response.status(Status.BAD_REQUEST).entity(errors).build();
		}
		
		return Response.ok("Senha alterada com sucesso.").build();
	}
	
	private User getUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return (User) authentication.getPrincipal();
	}
	
	@Path("/{id}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteUser(@PathParam("id") long id) {
		UserDto user = this.userFacade.findUser(id);
		if(user == null) {
			throw new WebApplicationException(Status.NOT_FOUND);
		}
		
		List<String> errors = this.userFacade.deleteUser(id);
		
		if(!errors.isEmpty()) {
			return Response.status(Status.BAD_REQUEST).entity(errors).build();
		}
		
		return Response.ok(new JSONArray()).build();
	}
	
	@Path("/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public UserDto getUser(@PathParam("id") long id) {
		UserDto dto = this.userFacade.findUser(id);
		
		if(dto == null) {
			throw new WebApplicationException(Status.NOT_FOUND);
		}
		
		return dto;
	}
	
	@Path("/")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<UserDto> getUsers() {
		return this.userFacade.listUsers();
	}
}
