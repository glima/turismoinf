package br.com.turismoinf.web.interfaceapi.dto.parse;

import br.com.turismoinf.web.domain.models.category.Category;
import br.com.turismoinf.web.interfaceapi.dto.CategoryDto;

public class CategoryDtoParse {

	public CategoryDto parseModelToDto(Category category) {
		CategoryDto dto = new CategoryDto();
		
		dto.setId(category.getId());
		dto.setDescription(category.getDescription());
		dto.setImage(category.getIcon());
		
		return dto;
	}
	
	public CategoryDto parseModelToDtoMinified(Category category) {
		CategoryDto dto = new CategoryDto();
		
		dto.setId(category.getId());
		dto.setDescription(category.getDescription());
		
		if(category.getIcon() != null && !category.getIcon().equals("")) {
			dto.setHasImage(true);
		}
		
		return dto;
	}
	
}
