package br.com.turismoinf.web;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Configuration
@SpringBootApplication
public class TurismInfApplication {
	
	private static final Logger logger = LoggerFactory.getLogger(TurismInfApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(TurismInfApplication.class, args);
		
		try {
			String path = new File(".").getCanonicalPath();
			File logFolder = new File(path + File.separator + "log");
			if(!logFolder.exists()) {
				if(logFolder.mkdir()) {
					logger.info("Diretorio de log criado");
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("Falha ao criar o diretorio de log");
		}
	}
	
	@Controller
	public class HomeController {
		@RequestMapping("/ui")
		public String home() {
			return "index";
		}
		
		@RequestMapping("/ui/login")
		public String login() {
			return "login";
		}
	}
}
