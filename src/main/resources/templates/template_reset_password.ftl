<html>
	<body>
		<h3 style="color: gray;">Ol&aacute; ${username}.</h3>
		
		<p style="color: black; margin-top: 10px;">
			Sua nova senha &eacute; <b>${newpass}</b>. Na pr&oacute;xima vez que logar sugerimos que altere sua senha.
		</p>
		
		<i style="margin-top: 10px;">Aten&ccedil;&atilde;o: Se n&atilde;o foi voc&ecirc; quem solicitou a redefini&ccedil;&atilde;o da senha sugerimos que altere a senha de seu email por quest&otilde;es de seguran&ccedil;a.</i><br>
		<i>OBS: Este email &eacute; apenas informativo, portanto favor n&atilde;o responder.</i><br><br>
		
	</body>
</html>